/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import model.Email;
import utilities.EmailUtils;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Random;
import model.User;
import org.apache.commons.lang3.RandomStringUtils;
/**
 *
 * @author kieuq
 */





public class ForgotPasswordServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
           response.sendRedirect("ForgotPassword.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            UserDAO dao= new UserDAO();
            String emailAdress=request.getParameter("email");
            String user_name=request.getParameter("user_name");
            String token=RandomStringUtils.randomAlphabetic(30);
            String resetPasswordLink = "http://localhost:8080/TrainingSupportSystem/reset_password?token=" + token;
            User user=dao.findByUserNameAndEmail(user_name, emailAdress);
            
            if(user==null){
                request.setAttribute("error", "User Name or email are incorrect!<br>");
                request.getRequestDispatcher("ForgotPassword.jsp").forward(request, response);
            }else{
                dao.saveToken(user.getEmail(), token);
                Email email=new Email();
                email.setFrom("quangtkhe151424@fpt.edu.vn");
                email.setFromPassword("Quangta260901@");
                email.setTo(emailAdress);
                email.setSubject("Forgot password");
                StringBuilder sb=new StringBuilder();
                sb.append("Dear ").append(user_name).append(",<br>");
                sb.append("Are you forgot password? Don't worry").append("<br>");
                //sb.append(resetPasswordLink).append("<br>");
                sb.append("This is link change your password: <b>").append("<a href=\""+resetPasswordLink+"\">Change my password</a>").append("</b><br>");
                sb.append("Regards<br>");
                sb.append("Adminatrator.");
                
                email.setContent(sb.toString());
                EmailUtils.send(email);
                request.setAttribute("message", "Email send to the email Address. Please check and get your password<br>");
                request.getRequestDispatcher("ForgotPassword.jsp").forward(request, response);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute("error", e.getMessage());
        }
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}