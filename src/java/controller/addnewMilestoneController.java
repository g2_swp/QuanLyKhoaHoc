/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import com.google.gson.Gson;
import dal.MilestoneDao;
import dto.ClassDTO;
import dto.MileStoneDTO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author luong
 */
public class addnewMilestoneController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet addnewMilestoneController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet addnewMilestoneController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MilestoneDao dao = new MilestoneDao();
        List<ClassDTO> listC = dao.getListC();
        request.setAttribute("listC", listC);
        request.getRequestDispatcher("addNewmilestone.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String idAss = request.getParameter("id");
        String classcode = request.getParameter("cc");
        String subjectid = request.getParameter("sid");
        String title = request.getParameter("tt");
        String fromdate = request.getParameter("fdate");
        String todate = request.getParameter("tdate");
        String des = request.getParameter("des");
        String ass_body = request.getParameter("abody");
        String statusM = request.getParameter("stt");

        MilestoneDao dao = new MilestoneDao();
        try {
            int classid = dao.classId(Integer.parseInt(subjectid), classcode);
            MileStoneDTO mi = dao.getMilestone(classid, Integer.parseInt(idAss));
            if (mi == null) {
                int a = dao.addNewmilestone(Integer.parseInt(idAss), classid, fromdate, todate, title, ass_body, des, Integer.parseInt(statusM));
                if (a > 0) {
                    int milestone_id = dao.milestoneId(Integer.parseInt(idAss), classid);
                    dao.addeVal_criteria(Integer.parseInt(idAss), milestone_id);
                    String mess = new Gson().toJson("ADD MILESTONES SUCCESSFUL!");
                    out.print(mess);
                    out.flush();
                } else {
                    String mess = new Gson().toJson("ADD MILESTONES FAIL!");
                    out.print(mess);
                    out.flush();
                }
            } else {
                String mess = new Gson().toJson("ADD MILESTONES FAIL!");
                out.print(mess);
                out.flush();
            }
        } catch (SQLException ex) {
            out.print("ADD MILESTONES FAIL!");
            out.flush();
            Logger.getLogger(addnewMilestoneController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
