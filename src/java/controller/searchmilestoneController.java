/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import com.google.gson.Gson;
import dal.MilestoneDao;
import dto.ClassDTO;
import dto.MileStoneDTO;
import dto.SubjectDTO;
import dto.UserDTO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author luong
 */
public class searchmilestoneController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet searchmilestoneController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet searchmilestoneController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("idmile");
        MilestoneDao dao = new MilestoneDao();
        MileStoneDTO m = dao.getMilestoneDetail(id);
        PrintWriter out = response.getWriter();
        if (m.getStatus() == 1) {
            try {
                dao.editmilestone(Integer.parseInt(id), 0);
                out.print("DeActive");
            } catch (SQLException ex) {
                Logger.getLogger(searchmilestoneController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                dao.editmilestone(Integer.parseInt(id), 1);
                out.print("Active");

            } catch (SQLException ex) {
                Logger.getLogger(searchmilestoneController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        MilestoneDao dao = new MilestoneDao();
        String title = request.getParameter("title");
        String fromdate = request.getParameter("fromdate");
        String todate = request.getParameter("todate");
        String classcode = request.getParameter("class");
        String status = request.getParameter("filterStatus");
        String index = request.getParameter("index");
        if (status.equals("2") || status.isEmpty()) {
            status = null;
        }
        if (fromdate.isEmpty()) {
            fromdate = null;
        }
        if (todate.isEmpty()) {
            todate = null;
        }
        if (classcode.isEmpty()) {
            classcode = null;
        }
        if (title.isEmpty()) {
            title = null;
        }
        out.print(title);
        out.print(fromdate);
        out.print(todate);
        out.print(classcode);
        out.print(status);
//        
        int totalp;
        int endP;
        totalp = dao.getTotalP(classcode, title, fromdate, todate, status);
        endP = totalp / 5;
        if (totalp % 5 != 0) {
            endP++;
        }
        if (index == null) {
            index = "1";
        }
        List<ClassDTO> listC = dao.getListC();
        request.setAttribute("listC", listC);
        ArrayList<MileStoneDTO> list = dao.getSearchListMilestone(Integer.parseInt(index), classcode, title, fromdate, todate, status);
        request.setAttribute("listMilestone", list);
        request.setAttribute("endP", endP);
        request.setAttribute("index", index);
        request.setAttribute("classcode", classcode);
        request.setAttribute("title", title);
        request.setAttribute("fromdate", fromdate);
        request.setAttribute("todate", todate);
        request.setAttribute("status", status);
        request.getRequestDispatcher("milestone.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
