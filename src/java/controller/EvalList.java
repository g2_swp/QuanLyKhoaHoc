/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.EvalDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.Assignment;
import model.Eval_criteria;

/**
 *
 * @author MEP
 */
public class EvalList extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session=request.getSession();
        List<Eval_criteria> listEval=(List<Eval_criteria>) session.getAttribute("listEval");
        
        int page, numberCont=10;
        int size=listEval.size();
        int numbOfPage=(size%numberCont==0?(size/numberCont):(size/numberCont)+1);
        String xpage=request.getParameter("page");
        if(xpage==null){
            page=1;
        } else {
            page=Integer.parseInt(xpage);
        }
        int start=(page-1)*numberCont;
        int end=Math.min(page*numberCont, size);
        EvalDAO eval = new EvalDAO();
        List<Eval_criteria> list=eval.getListByPage((ArrayList<Eval_criteria>) listEval, start, end);
        List<Assignment> listass = eval.getAssTitle();
        session.setAttribute("listass", listass);
        session.setAttribute("listEval", list);
        request.setAttribute("page", page);
        request.setAttribute("numP", numbOfPage);
        request.getRequestDispatcher("eval-list.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        EvalDAO eval = new EvalDAO();
        List<Eval_criteria> listEval = eval.getAllEval();
        HttpSession session=request.getSession();
        session.setAttribute("listEval", listEval);
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String ass_id = request.getParameter("ass_id");
        EvalDAO eval = new EvalDAO();
        List<Eval_criteria> listEval = eval.FilterEvalByAssignment(ass_id);
        HttpSession session=request.getSession();
        session.setAttribute("listEval", listEval);

        try {
            int AssID = Integer.parseInt(ass_id);
            request.setAttribute("AssID", AssID);
        } catch (Exception e) {
        }
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
