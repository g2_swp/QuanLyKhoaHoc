/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.SubjectDAO;
import dto.SubjectDTO;
import dto.UserDTO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author luong
 */
public class editController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SubjectDAO dao = new SubjectDAO();
        String id = request.getParameter("id");
        SubjectDTO s = dao.getSubjectDetail(id);
        request.setAttribute("subject", s);
        List<UserDTO> uM = dao.getListUser1(21);
        List<UserDTO> uE = dao.getListUser1(22);
        String nota = request.getParameter("action") ;
       if(request.getParameter("action") != null){
           request.setAttribute("nota", "Edit Subject Sucessfully !");
       }
        request.setAttribute("userListM", uM);
        request.setAttribute("userListE", uE);
        request.getRequestDispatcher("editSubject.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        SubjectDAO dao = new SubjectDAO();
        String id = request.getParameter("subjectid");
        String code = request.getParameter("code");
        String name = request.getParameter("name");
        String manager = request.getParameter("manager");
        String expert = request.getParameter("expert");
        String status = request.getParameter("status");
        String des = request.getParameter("des");
        try {
            dao.editSubject(Integer.parseInt(id), code, name, manager, expert, Integer.parseInt(status), des);
            response.sendRedirect("editController?action=ok&id="+id);
        } catch (SQLException ex) {
            Logger.getLogger(editController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
