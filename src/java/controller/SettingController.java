/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.SettingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Setting;

/**
 *
 * @author Hung
 */
public class SettingController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE  html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SettingController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SettingController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int setting_id = Integer.parseInt(request.getParameter("setting_id"));
        SettingDAO SettingDAO = new SettingDAO();
        Setting s = SettingDAO.getSetting(setting_id);
        ArrayList<Setting> type_title_list = SettingDAO.getAllSettingType();
        request.setAttribute("type_title_list", type_title_list);
        request.setAttribute("setting_details", s);
        request.getRequestDispatcher("SettingDetails.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int setting_id = Integer.parseInt(request.getParameter("setting_id"));
        int type_id = Integer.parseInt(request.getParameter("type_id"));
        String setting_title = request.getParameter("setting_title");
        String setting_value = request.getParameter("setting_value");
        int display_order = Integer.parseInt(request.getParameter("display_order"));
        int status = Integer.parseInt(request.getParameter("status"));
        String description = request.getParameter("description");
        SettingDAO SettingDAO = new SettingDAO();
        SettingDAO.editSetting(new Setting(setting_id, type_id, setting_title, setting_value, display_order, status, description));
        request.getRequestDispatcher("SettingList").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
