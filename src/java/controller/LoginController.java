/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import utilities.VerifyRecaptcha;
import dal.RoleDAO;
import dal.UserDAO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Role;
import model.User;
import static utilities.GetMD5.getMD5;

/**
 *
 * @author Hung
 */
public class LoginController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String md5 = getMD5(password);
        UserDAO UserDAO = new UserDAO();
        ArrayList<User> userList = new ArrayList<>();
        userList = UserDAO.getAll();
        RoleDAO RoleDAO = new RoleDAO();
        ArrayList<Role> roleList = new ArrayList<>();
        roleList = RoleDAO.getAll();
        boolean is_exist = false;
        String gRecaptchaResponse = request
                .getParameter("g-recaptcha-response");
        System.out.println(gRecaptchaResponse);
        boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);
        for (User u : userList) {
            if (u.getEmail().equals(email) && u.getPassword().equals(md5)) {
                HttpSession session = request.getSession();
                session.setAttribute("user_id", u.getUser_id());
                session.setAttribute("email", u.getEmail());
                session.setAttribute("user_name", u.getUser_name());
                session.setAttribute("full_name", u.getFull_name());
                session.setAttribute("avatar_url", u.getAvatar_url());
                session.setAttribute("mobile", u.getMobile());
                ArrayList roleListOfCurrentUser = RoleDAO.getRoleListOfAnUser(u.getUser_id());
                session.setAttribute("roleListOfCurrentUser", roleListOfCurrentUser);
                is_exist = true;
            }
        }
        if (is_exist == false || verify == false) {
            RequestDispatcher rd = getServletContext().getRequestDispatcher(
                    "/login.jsp");
            PrintWriter out = response.getWriter();
            if (verify) {
                request.setAttribute("message", "Either email or password is wrong.");
            } else {
                request.setAttribute("message", "You missed the Captcha.");
            }
            rd.include(request, response);
        } else if (is_exist == true && verify == true) {
            request.getRequestDispatcher("HOME").include(request, response);

        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    
}
