/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.SettingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Setting;

/**
 *
 * @author Hung
 */
public class SettingList extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE h  tml>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Setting_List</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Setting_List at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        SettingDAO SettingDAO = new SettingDAO();
//        int count_setting_number = SettingDAO.countSettingsNumber();
//        int setting_page_number = 0;
//        HttpSession session = request.getSession();
//        if (count_setting_number % 10 == 0) {
//            setting_page_number = (count_setting_number / 10);
//        } else {
//            setting_page_number = (count_setting_number / 10) + 1;
//        }
//        session.setAttribute("count_setting_number", count_setting_number);
//        session.setAttribute("setting_page_number", setting_page_number);
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String operation = null;
        operation = request.getParameter("operation");
        if (operation != null && operation.equals("delete_setting")) {
            DeleteSetting(request,response);
            response.sendRedirect("SettingList");
        }
        if (operation != null && operation.equals("add_new_setting")) {
            AddNewSetting(request, response);
            response.sendRedirect("SettingList");
        }
        if (operation != null && operation.equals("wiew_add_setting_page")) {
            ViewAdd(request, response);
            request.getRequestDispatcher("AddNewSetting.jsp").forward(request, response);
        }
        if (operation != null && operation.equals("search")) {
            Search(request, response);
            request.getRequestDispatcher("/SettingList.jsp").include(request, response);
        } else {
            SettingDAO SettingDAO = new SettingDAO();
            HttpSession session = request.getSession();
            ArrayList<Setting> setting_types = SettingDAO.getAllSettingType();
            session.setAttribute("setting_types", setting_types);
            int count_setting_number = SettingDAO.countSettingsNumber();
            int setting_page_number = 0;

            if (count_setting_number % 10 == 0) {
                setting_page_number = (count_setting_number / 10);
            } else {
                setting_page_number = (count_setting_number / 10) + 1;
            }
            session.setAttribute("count_setting_number", count_setting_number);
            session.setAttribute("setting_page_number", setting_page_number);

            int current_page = 1;
            String sort_column = null;

            try {
                current_page = (int) request.getAttribute("current_page");
            } catch (Exception e) {

            }
            try {
                current_page = Integer.parseInt(request.getParameter("current_page"));
            } catch (Exception e) {

            }
            try {
                sort_column = (String) session.getAttribute("sort_column");
            } catch (Exception e) {

            }
            try {
                sort_column = request.getParameter("sort_column");
                if (sort_column == null) {
                    sort_column = (String) session.getAttribute("sort_column");
                }
            } catch (Exception e) {

            }

            if (sort_column == null) {
                sort_column = "setting_id";
            }
            session.setAttribute("sort_column", sort_column);
            ArrayList<Setting> setting_list = new ArrayList();
            setting_list = SettingDAO.getSomeSettingList((current_page - 1) * 10, sort_column);
            request.setAttribute("setting_list", setting_list);
            request.setAttribute("current_page", current_page);
            request.getRequestDispatcher("/SettingList.jsp").include(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    protected void Search(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String findTitle = request.getParameter("findTitle");
        int filterStatus = Integer.parseInt(request.getParameter("filterStatus"));
        int filterType = Integer.parseInt(request.getParameter("filterType"));

        String sql_query = "select * from setting";

        if (findTitle != "") {
            sql_query += " where setting_title like '%" + findTitle + "%'";
            if (filterStatus != 2) {
                sql_query += " and status=" + filterStatus;
            }
            if (filterType != 0) {
                sql_query += " and type_id=" + filterType;
            }
        } else {
            if (filterStatus != 2) {
                sql_query += " where status=" + filterStatus;
                if (filterType != 0) {
                    sql_query += " and type_id=" + filterType;
                }
            } else {
                if (filterType != 0) {
                    sql_query += " where type_id=" + filterType;
                }
            }

        }

        SettingDAO SettingDAO = new SettingDAO();
        ArrayList<Setting> setting_list = new ArrayList<>();
        setting_list = SettingDAO.searchAndFilter(sql_query);
        request.setAttribute("setting_list", setting_list);
    }

    private void ViewAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SettingDAO SettingDAO = new SettingDAO();
        int max_setting_id = SettingDAO.max_id();
        int new_setting_id = max_setting_id + 1;
        request.setAttribute("setting_id", new_setting_id);
        ArrayList<Setting> type_title_list = SettingDAO.getAllSettingType();
        request.setAttribute("type_title_list", type_title_list);

    }

    private void AddNewSetting(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int setting_id = Integer.parseInt(request.getParameter("setting_id"));
        int type_id = Integer.parseInt(request.getParameter("type_id"));
        String setting_title = request.getParameter("setting_title");
        String setting_value = request.getParameter("setting_value");
        int display_order = Integer.parseInt(request.getParameter("display_order"));
        int status = Integer.parseInt(request.getParameter("status"));
        String description = request.getParameter("description");
        SettingDAO SettingDAO = new SettingDAO();
        SettingDAO.addNewSetting(new Setting(setting_id, type_id, setting_title, setting_value, display_order, status, description));
    }

    private void DeleteSetting(HttpServletRequest request, HttpServletResponse response) {
        int setting_id = Integer.parseInt(request.getParameter("setting_id"));
        SettingDAO SettingDAO = new SettingDAO();
        SettingDAO.deleteSetting(setting_id);
    }
}
