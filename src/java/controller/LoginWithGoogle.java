package controller;

import dal.UserDAO;
import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.User;

import utilities.GooglePojo;
import utilities.GoogleUtils;

public class LoginWithGoogle extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public LoginWithGoogle() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String code = request.getParameter("code");

        if (code == null || code.isEmpty()) {
            RequestDispatcher dis = request.getRequestDispatcher("login.jsp");
            dis.include(request, response);
        } else {
            String accessToken = GoogleUtils.getToken(code);
            GooglePojo googlePojo = GoogleUtils.getUserInfo(accessToken);
            request.setAttribute("id", googlePojo.getId());
            request.setAttribute("name", googlePojo.getName());
            request.setAttribute("email", googlePojo.getEmail());

            UserDAO UserDAO = new UserDAO();
            User u = UserDAO.findByUserEmail(googlePojo.getEmail());
            if (u == null) {
                int user_id = UserDAO.max_id() + 1;
                String email = googlePojo.getEmail();
                User new_user = new User(user_id, null, null, email, null, null, null, null, null);
                UserDAO.addNewUser(new_user);
                HttpSession session = request.getSession();
                session.setAttribute("user_id", user_id);
                session.setAttribute("email", u.getEmail());
                request.getRequestDispatcher("HOME").include(request, response);
            }
            else{
                int user_id = u.getUser_id();
                HttpSession session = request.getSession();
                String email = u.getEmail();
                session.setAttribute("user_id", user_id);
                session.setAttribute("email", u.getEmail());
                request.getRequestDispatcher("HOME").include(request, response);
            }

        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
