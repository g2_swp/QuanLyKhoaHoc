/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import utilities.validate;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.User;

/**
 *
 * @author kieuq
 */
public class ChangePasswordServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        System.out.println(session+"get");
           if (session.getAttribute("user_id") != null) {
                response.sendRedirect("ChangePassword.jsp");
            } else {
                response.sendRedirect("Login");
            }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            HttpSession session = request.getSession();
            validate val = new validate();
            UserDAO dao = new UserDAO();
            System.out.println(session+"post");
            String user_id = session.getAttribute("user_id").toString();
            User u = dao.findByUserId(user_id);
            String new_Pass1 = request.getParameter("password1");
            String new_Pass2 = request.getParameter("password2");
            if (u != null && new_Pass1.equals(new_Pass2)) {
                dao.updatePasswordByEmail(val.getMd5(new_Pass1), u.getEmail());
                request.setAttribute("message", "Change Password success!");
               
            } else if (!new_Pass1.equals(new_Pass2)) {
                request.setAttribute("error", "Can not change,Passwords don't match!");
                
            } else {
                request.setAttribute("error", "Please login first!");
            }
        } catch (Exception e) {
        }
        request.getRequestDispatcher("ChangePassword.jsp").forward(request, response);
    }

}
