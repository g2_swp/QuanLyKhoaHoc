/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.SubjectDAO;
import dto.SubjectDTO;
import dto.UserDTO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author luong
 */
public class addController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet addController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet addController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SubjectDAO dao = new SubjectDAO();
        String code = request.getParameter("code");
        String name = request.getParameter("name");
        String manager = request.getParameter("manager");
        String expert = request.getParameter("expert");
        String status = request.getParameter("status");
        String des = request.getParameter("des");

        List<UserDTO> uM = dao.getListUser1(21);
        List<UserDTO> uE = dao.getListUser1(22);
        String nota = request.getParameter("action");
        if (request.getParameter("action") != null) {
            request.setAttribute("nota", "Add Subject Sucessfully !");
        }
        if(status != null){
            request.setAttribute("status", status);
        }
        if(code != null){
            request.setAttribute("code", code);
        }
        if(name != null){
            request.setAttribute("name", name);
        }
        if(manager != null){
            request.setAttribute("manager", manager);
        }
        if(expert != null){
            request.setAttribute("expert", expert);
        }
        if(des != null){
            request.setAttribute("des", des);
        }
        request.setAttribute("userListM", uM);
        request.setAttribute("userListE", uE);
        request.getRequestDispatcher("addSubject.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SubjectDAO dao = new SubjectDAO();
        String code = request.getParameter("code");
        String name = request.getParameter("name");
        String manager = request.getParameter("manager");
        String expert = request.getParameter("expert");
        String status = request.getParameter("status");
        String des = request.getParameter("des");
//        PrintWriter out = response.getWriter();
//        out.print(code);
//        out.print(name);
//        out.print(manager);
//        out.print(expert);
//        out.print(status);
//        out.print(des);
        try {
            dao.addSubject(code, name, Integer.parseInt(manager), Integer.parseInt(expert), Integer.parseInt(status), des);
            response.sendRedirect("addController?action=ok&manager="
                    + manager + "&expert=" + expert + "&name=" + name + "&code=" + code 
                    + "&status=" + status + "&des=" + des);
        } catch (SQLException ex) {
            Logger.getLogger(editController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
