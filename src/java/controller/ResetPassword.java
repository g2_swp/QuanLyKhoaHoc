/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.User;
import utilities.validate;

/**
 *
 * @author kieuq
 */
public class ResetPassword extends HttpServlet {


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         try {
            UserDAO dao=new UserDAO();
            String token = request.getParameter("token");
            System.out.println("token:"+token);
        //tim nguoi can update theo token
            User user=dao.findUserByToken(token);
            if(user==null){
                request.setAttribute("error", "Can't find user by your token, Check in your mail");
                response.sendRedirect("ForgotPassword.jsp");
                System.out.println("Khong tim thay get");
            }else{
                request.setAttribute("token", token);
                System.out.println("send token:"+token);
                request.setAttribute("user", user);
                request.getRequestDispatcher("UpdatePassword.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       try {
        validate val = new validate();
        UserDAO dao=new UserDAO();
        if(request.getParameter("token2")!=null){
        String token = request.getParameter("token2");
        String password = request.getParameter("password");
        System.out.println("token do post:"+token);
        //tim nguoi can update theo token
        User user=dao.findUserByToken(token);
        if(user==null){
            System.out.println("Can not find user by your token.");
            request.setAttribute("error", "Can not find user by your token.<br>");
            request.getRequestDispatcher("ForgotPassword.jsp").forward(request, response);
        }else{
            dao.updatePasswordByToken(val.getMd5(password), token);
            System.out.println("Change pass successfuly");
            //reset status 
            dao.saveToken(user.getEmail(), null);
            System.out.println("Delete token.");
            request.setAttribute("message", "Change password successfuly!<br>");
           request.getRequestDispatcher("ForgotPassword.jsp").forward(request, response);
        }
        }
        else{
            System.out.println("token null roi");
            request.setAttribute("error", "Token null!<br>");
            request.getRequestDispatcher("ForgotPassword.jsp").forward(request, response);
        }
 
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
