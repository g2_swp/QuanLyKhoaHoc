/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author MEP
 */
public class Contact {
    int contact_id;
    Setting setting;
    int staff_id;
    String full_name;
    String email;
    String mobile;
    String message;
    String response;
    int status;
    
    public Contact(){
        
    }

    public Contact(Setting setting, String full_name, String email, String mobile, String message) {
        this.setting = setting;
        this.full_name = full_name;
        this.email = email;
        this.mobile = mobile;
        this.message = message;
    }

    public Contact(int contact_id, Setting setting, int staff_id, String full_name, String email, String mobile, String message, String response, int status) {
        this.contact_id = contact_id;
        this.setting = setting;
        this.staff_id = staff_id;
        this.full_name = full_name;
        this.email = email;
        this.mobile = mobile;
        this.message = message;
        this.response = response;
        this.status = status;
    }

    public int getContact_id() {
        return contact_id;
    }

    public void setContact_id(int contact_id) {
        this.contact_id = contact_id;
    }

    public Setting getSetting() {
        return setting;
    }

    public void setSetting(Setting setting) {
        this.setting = setting;
    }

    public int getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(int staff_id) {
        this.staff_id = staff_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
