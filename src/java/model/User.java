/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Hung
 */
public class User {
    int user_id;
    String user_name;
    String full_name;
    String email;
    String mobile;
    String password;
    String avatar_url;
    String status;
    String note;

    public User() {
    }

    public User(int user_id, String user_name, String full_name, String email, String mobile, String password, String avatar_url, String status, String note) {
        this.user_id = user_id;
        this.user_name = user_name;
        this.full_name = full_name;
        this.email = email;
        this.mobile = mobile;
        this.password = password;
        this.avatar_url = avatar_url;
        this.status = status;
        this.note = note;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

//    public User(int user_id, String full_name, String email, String mobile, String password, String avatar_url, String status, String note) {
//        this.user_id = user_id;
//        this.full_name = full_name;
//        this.email = email;
//        this.mobile = mobile;
//        this.password = password;
//        this.avatar_url = avatar_url;
//        this.status = status;
//        this.note = note;
//    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
    
}
