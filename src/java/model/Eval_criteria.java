/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author MEP
 */
public class Eval_criteria {
    int criteria_id;
    Assignment assignment;
    int milestone_id;
    String criteria_name;
    String is_team_eval;
    String eval_weight;
    int max_loc;
    int status;
    String description;

    public Eval_criteria() {
    }

    public Eval_criteria(int criteria_id, Assignment assignment, int milestone_id, String criteria_name, String is_team_eval, String eval_weight, int max_loc, int status, String description) {
        this.criteria_id = criteria_id;
        this.assignment = assignment;
        this.milestone_id = milestone_id;
        this.criteria_name = criteria_name;
        this.is_team_eval = is_team_eval;
        this.eval_weight = eval_weight;
        this.max_loc = max_loc;
        this.status = status;
        this.description = description;
    }

    public int getCriteria_id() {
        return criteria_id;
    }

    public void setCriteria_id(int criteria_id) {
        this.criteria_id = criteria_id;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    public int getMilestone_id() {
        return milestone_id;
    }

    public void setMilestone_id(int milestone_id) {
        this.milestone_id = milestone_id;
    }

    public String getCriteria_name() {
        return criteria_name;
    }

    public void setCriteria_name(String criteria_name) {
        this.criteria_name = criteria_name;
    }

    public String getIs_team_eval() {
        return is_team_eval;
    }

    public void setIs_team_eval(String is_team_eval) {
        this.is_team_eval = is_team_eval;
    }

    public String getEval_weight() {
        return eval_weight;
    }

    public void setEval_weight(String eval_weight) {
        this.eval_weight = eval_weight;
    }

    public int getMax_loc() {
        return max_loc;
    }

    public void setMax_loc(int max_loc) {
        this.max_loc = max_loc;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    
    
    
    
}
