/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

/**
 *
 * @author luong
 */
public class Subject2DTO {
    int subject_id;
    String subject_code;

    public Subject2DTO() {
    }

    public Subject2DTO(int subject_id, String subject_code) {
        this.subject_id = subject_id;
        this.subject_code = subject_code;
    }

    public int getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(int subject_id) {
        this.subject_id = subject_id;
    }

    public String getSubject_code() {
        return subject_code;
    }

    public void setSubject_code(String subject_code) {
        this.subject_code = subject_code;
    }
    
}
