/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

/**
 *
 * @author luong
 */
public class AssignmentDTO {

    int ass_id;
    String subjectName;
    String title;
    String ass_body;
    int eval_weight;
    int is_going;
    int is_team_work;
    int status;
    int subject_id;

    public AssignmentDTO() {
    }

    public AssignmentDTO(int ass_id, String subjectName, String title, String ass_body, int eval_weight, int is_going, int is_team_work, int status, int subject_id) {
        this.ass_id = ass_id;
        this.subjectName = subjectName;
        this.title = title;
        this.ass_body = ass_body;
        this.eval_weight = eval_weight;
        this.is_going = is_going;
        this.is_team_work = is_team_work;
        this.status = status;
        this.subject_id = subject_id;
    }

    public int getAss_id() {
        return ass_id;
    }

    public void setAss_id(int ass_id) {
        this.ass_id = ass_id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAss_body() {
        return ass_body;
    }

    public void setAss_body(String ass_body) {
        this.ass_body = ass_body;
    }

    public int getEval_weight() {
        return eval_weight;
    }

    public void setEval_weight(int eval_weight) {
        this.eval_weight = eval_weight;
    }

    public int getIs_going() {
        return is_going;
    }

    public void setIs_going(int is_going) {
        this.is_going = is_going;
    }

    public int getIs_team_work() {
        return is_team_work;
    }

    public void setIs_team_work(int is_team_work) {
        this.is_team_work = is_team_work;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(int subject_id) {
        this.subject_id = subject_id;
    }

    @Override
    public String toString() {
        return "AssignmentDTO{" + "ass_id=" + ass_id + ", subject_id=" + subject_id + ", subjectName=" + subjectName + ", title=" + title + ", ass_body=" + ass_body + ", eval_weight=" + eval_weight + ", is_going=" + is_going + ", is_team_work=" + is_team_work + ", status=" + status + '}';
    }
}
