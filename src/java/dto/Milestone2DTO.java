/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

/**
 *
 * @author luong
 */
public class Milestone2DTO {
     int milestone_id;
    String titleAss;
    int assid;
    String class_code;
    int classid;
    String from_date;
    String to_date;
    String titleMilestone;
    String ass_body;
    String description;
    int status;

    public Milestone2DTO() {
    }

    public Milestone2DTO(int milestone_id, String titleAss, int assid, String class_code, int classid, String from_date, String to_date, String titleMilestone, String ass_body, String description, int status) {
        this.milestone_id = milestone_id;
        this.titleAss = titleAss;
        this.assid = assid;
        this.class_code = class_code;
        this.classid = classid;
        this.from_date = from_date;
        this.to_date = to_date;
        this.titleMilestone = titleMilestone;
        this.ass_body = ass_body;
        this.description = description;
        this.status = status;
    }

    public int getMilestone_id() {
        return milestone_id;
    }

    public void setMilestone_id(int milestone_id) {
        this.milestone_id = milestone_id;
    }

    public String getTitleAss() {
        return titleAss;
    }

    public void setTitleAss(String titleAss) {
        this.titleAss = titleAss;
    }

    public int getAssid() {
        return assid;
    }

    public void setAssid(int assid) {
        this.assid = assid;
    }

    public String getClass_code() {
        return class_code;
    }

    public void setClass_code(String class_code) {
        this.class_code = class_code;
    }

    public int getClassid() {
        return classid;
    }

    public void setClassid(int classid) {
        this.classid = classid;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getTitleMilestone() {
        return titleMilestone;
    }

    public void setTitleMilestone(String titleMilestone) {
        this.titleMilestone = titleMilestone;
    }

    public String getAss_body() {
        return ass_body;
    }

    public void setAss_body(String ass_body) {
        this.ass_body = ass_body;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
}
