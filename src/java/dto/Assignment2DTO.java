/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

/**
 *
 * @author luong
 */
public class Assignment2DTO {

    int ass_id;
    String subjectName;

    public Assignment2DTO() {
    }

    public Assignment2DTO(int ass_id, String subjectName) {
        this.ass_id = ass_id;
        this.subjectName = subjectName;
    }

    public int getAss_id() {
        return ass_id;
    }

    public void setAss_id(int ass_id) {
        this.ass_id = ass_id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

}
