/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

/**
 *
 * @author luong
 */
public class SubjectDTO {

    private int subject_id;
    private String subject_code;
    private String subject_name;
    private String manager;
    private String expert;
    private int status;
    private String body;

    public SubjectDTO() {
    }

    public SubjectDTO(int subject_id, String subject_code, String subject_name, String manager, String expert, int status, String body) {
        this.subject_id = subject_id;
        this.subject_code = subject_code;
        this.subject_name = subject_name;
        this.manager = manager;
        this.expert = expert;
        this.status = status;
        this.body = body;
    }

    public int getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(int subject_id) {
        this.subject_id = subject_id;
    }

    public String getSubject_code() {
        return subject_code;
    }

    public void setSubject_code(String subject_code) {
        this.subject_code = subject_code;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getExpert() {
        return expert;
    }

    public void setExpert(String expert) {
        this.expert = expert;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "SubjectDTO{" + "subject_id=" + subject_id + ", subject_code=" + subject_code + ", subject_name=" + subject_name + ", manager=" + manager + ", expert=" + expert + ", status=" + status + ", body=" + body + '}';
    }
    
    
}
