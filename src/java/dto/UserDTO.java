/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

/**
 *
 * @author luong
 */
public class UserDTO {
    private int user_id;
    private String fullname;

    public UserDTO() {
    }

    public UserDTO(int user_id, String fullname) {
        this.user_id = user_id;
        this.fullname = fullname;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    @Override
    public String toString() {
        return "UserDTO{" + "user_id=" + user_id + ", fullname=" + fullname + '}';
    }
    
}
