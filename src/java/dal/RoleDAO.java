/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Role;

/**
 *
 * @author Hung
 */
public class RoleDAO {

    public ArrayList<Role> getAll() {
        ArrayList<Role> List = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT user.user_id,setting.setting_title as role\n"
                    + "FROM user\n"
                    + "INNER JOIN user_role ON user.user_id = user_role.user_id\n"
                    + "INNER JOIN setting ON user_role.role_id = setting.setting_id";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Role s = new Role(
                        rs.getInt("user_id"),
                        rs.getString("role")
                );
                List.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return List;
    }
    public ArrayList<Role> getRoleListOfAnUser(int user_id) {
        ArrayList<Role> List = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT user.user_id,setting.setting_title as role\n"
                    + "FROM user\n"
                    + "INNER JOIN user_role ON user.user_id = user_role.user_id\n"
                    + "INNER JOIN setting ON user_role.role_id = setting.setting_id where user.user_id=?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, user_id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Role s = new Role(
                        rs.getInt("user_id"),
                        rs.getString("role")
                );
                List.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return List;
    }
}
