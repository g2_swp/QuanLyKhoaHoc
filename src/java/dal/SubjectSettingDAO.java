/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Setting;
import model.SubjectSetting;

/**
 *
 * @author hungl
 */
public class SubjectSettingDAO {



    public int countSettingsNumber() {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT COUNT(*) as count FROM subject_setting;";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                int s = rs.getInt("count");
                return s;
            }

        } catch (SQLException ex) {
            Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public ArrayList<SubjectSetting> getSomeSettingList(int start, String sort_column) {
        ArrayList<SubjectSetting> List = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "select * from subject_setting order by " + sort_column + " asc LIMIT " + start + ", 10";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                SubjectSetting s = new SubjectSetting(
                        rs.getInt("setting_id"),
                        rs.getInt("subject_id"),
                        rs.getInt("type_id"),
                        rs.getString("setting_title"),
                        rs.getString("setting_value"),
                        rs.getInt("display_order"),
                        rs.getInt("status"),
                        rs.getString("description")
                );
                List.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return List;
    }

    public ArrayList<Setting> getAllSubjectSettingType() {
        ArrayList<Setting> List = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "Select * from setting where type_id = 4";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Setting s = new Setting(
                        rs.getInt("setting_id"),
                        rs.getInt("type_id"),
                        rs.getString("setting_title"),
                        rs.getString("setting_value"),
                        rs.getInt("display_order"),
                        rs.getInt("status"),
                        rs.getString("description")
                );
                List.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return List;
    }

    public ArrayList<SubjectSetting> searchAndFilter(String sql_query) {
        ArrayList<SubjectSetting> List = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = sql_query;
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                SubjectSetting s = new SubjectSetting(
                        rs.getInt("setting_id"),
                        rs.getInt("subject_id"),
                        rs.getInt("type_id"),
                        rs.getString("setting_title"),
                        rs.getString("setting_value"),
                        rs.getInt("display_order"),
                        rs.getInt("status"),
                        rs.getString("description")
                );
                List.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return List;
    }

    public SubjectSetting getSetting(int setting_id) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT * FROM subject_setting where setting_id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, setting_id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                SubjectSetting s = new SubjectSetting(
                        rs.getInt("setting_id"),
                        rs.getInt("subject_id"),
                        rs.getInt("type_id"),
                        rs.getString("setting_title"),
                        rs.getString("setting_value"),
                        rs.getInt("display_order"),
                        rs.getInt("status"),
                        rs.getString("description")
                );
                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void editSetting(SubjectSetting s) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "UPDATE subject_setting SET subject_id = ?, type_id = ?, setting_title=?,setting_value=?,display_order=?, status=?, description=? WHERE setting_id = ?;";
            PreparedStatement statement = connection.prepareStatement(sql);

            statement.setInt(1, s.getSubject_id());
            statement.setInt(2, s.getType_id());
            statement.setString(3, s.getSetting_title());
            statement.setString(4, s.getSetting_value());
            statement.setInt(5, s.getDisplay_order());
            statement.setInt(6, s.getStatus());
            statement.setString(7, s.getDescription());
            statement.setInt(8, s.getSetting_id());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int max_id() {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT max(setting_id) as max from subject_setting";

            PreparedStatement statement = connection.prepareStatement(sql);

            ResultSet rs = statement.executeQuery();
            int s = 0;
            if (rs.next()) {
                s = rs.getInt("max");
                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public void addNewSetting(SubjectSetting s) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "insert into subject_setting (setting_id, subject_id, type_id, setting_title, setting_value, display_order, status, description)\n"
                    + "values(?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, s.getSetting_id());
            statement.setInt(2, s.getSubject_id());
            statement.setInt(3, s.getType_id());
            statement.setString(4, s.getSetting_title());
            statement.setString(5, s.getSetting_value());
            statement.setInt(6, s.getDisplay_order());
            statement.setInt(7, s.getStatus());
            statement.setString(8, s.getDescription());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteSetting(int setting_id) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "delete FROM subject_setting where setting_id=?;";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, setting_id);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void changeStatus(int setting_id, int status) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "UPDATE subject_setting SET status=? WHERE setting_id = ?;";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, status);
            statement.setInt(2, setting_id);

            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
