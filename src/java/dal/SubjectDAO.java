/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import dto.SubjectDTO;
import dto.UserDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Subject;

/**
 *
 * @author luong
 */
public class SubjectDAO {

    public ArrayList<SubjectDTO> getSearchAll(String option, String key, String manager, String expert, String status, int index) {
        ArrayList<SubjectDTO> listSubject = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            List<UserDTO> a = getListUser();
            StringBuilder condition = new StringBuilder();
            String sql = "SELECT subject_id,subject_code,subject_name,(select full_name from user where user_id = manager_id) as manager ,\n"
                    + "(select full_name from user where user_id = expert_id) as expert,status,body FROM swp_project.subject [CONDITION]"
                    + " LIMIT " + (index * 10 - 10) + ", 10";
//            StringBuilder strBuilderQuery = new StringBuilder(sql);

            if (option != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                if (option.equals("code")) {
                    condition.append(" subject_code like \"%" + key + "%\"");
                }
                if (option.equals("name")) {
                    condition.append(" subject_name like \"%" + key + "%\"");
                }
            }
            if (manager != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                condition.append("(select full_name from user where user_id = manager_id) = \"" + manager + "\" ");
            }
            if (expert != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                condition.append("(select full_name from user where user_id = expert_id) = \"" + expert + "\" ");
            }
            if (status != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                condition.append("status = \"" + status + "\"");
            }

            if (condition.length() != 0) {
                condition.insert(0, " where ");
            }
            sql = sql.replace("[CONDITION]", condition.toString());
            PreparedStatement statement = connection.prepareStatement(sql.toString());
            ResultSet rs = statement.executeQuery();
            System.out.println(sql);
            while (rs.next()) {
                SubjectDTO s = new SubjectDTO(
                        rs.getInt("subject_id"),
                        rs.getString("subject_code"),
                        rs.getString("subject_name"),
                        rs.getString("manager"),
                        rs.getString("expert"),
                        rs.getInt("status"),
                        rs.getString("body")
                );
                listSubject.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listSubject;
    }

    public int getCountSearchAll(String option, String key, String manager, String expert, String status) {
        ArrayList<SubjectDTO> listSubject = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            List<UserDTO> a = getListUser();
            String sql
                    = " select count(*) FROM swp_project.subject";
            StringBuilder strBuilderQuery = new StringBuilder(sql);
            StringBuilder condition = new StringBuilder("");
            if (option != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                if (option.equals("code")) {
                    condition.append(" subject_code like \"%" + key + "%\"");
                }
                if (option.equals("name")) {
                    condition.append(" subject_name like \"%" + key + "%\"");
                }
            }
            if (manager != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                condition.append("(select full_name from user where user_id = manager_id) = \"" + manager + "\" ");
            }
            if (expert != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                condition.append("(select full_name from user where user_id = expert_id) = \"" + expert + "\" ");
            }
            if (status != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                condition.append("status = \"" + status + "\"");
            }

            if (condition.length() != 0) {
                condition.insert(0, " where ");
                // condition.append(" order by mo.order_time desc");
                strBuilderQuery.append(condition);
            }
            PreparedStatement statement = connection.prepareStatement(strBuilderQuery.toString());
            ResultSet rs = statement.executeQuery();
            System.out.println(strBuilderQuery);
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public ArrayList<SubjectDTO> sortgetAll(String order, String sort, int index) {
        ArrayList<SubjectDTO> listSubject = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            List<UserDTO> a = getListUser();
            int ind = (index * 10 - 10);
            String orderBy = createOrderBy(order, sort);
            String sql = "SELECT subject_id,subject_code,subject_name,(select full_name from user where user_id = manager_id) as manager ,\n"
                    + "(select full_name from user where user_id = expert_id) as expert,status,body FROM swp_project.subject " + orderBy + " LIMIT " + ind + ", 10";
            System.out.println(sql);

            StringBuilder strBuilderQuery = new StringBuilder(sql);

            PreparedStatement statement = connection.prepareStatement(strBuilderQuery.toString());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                SubjectDTO s = new SubjectDTO(
                        rs.getInt("subject_id"),
                        rs.getString("subject_code"),
                        rs.getString("subject_name"),
                        rs.getString("manager"),
                        rs.getString("expert"),
                        rs.getInt("status"),
                        rs.getString("body")
                );
                listSubject.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listSubject;
    }

    public ArrayList<SubjectDTO> pagin(int index) {
        ArrayList<SubjectDTO> listSubject = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            List<UserDTO> a = getListUser();
            String sql = "SELECT subject_id,subject_code,subject_name,(select full_name from user where user_id = manager_id) as manager ,\n"
                    + "(select full_name from user where user_id = expert_id) as expert,status,body FROM swp_project.subject LIMIT " + (index * 10 - 10) + ", 10";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                SubjectDTO s = new SubjectDTO(
                        rs.getInt("subject_id"),
                        rs.getString("subject_code"),
                        rs.getString("subject_name"),
                        rs.getString("manager"),
                        rs.getString("expert"),
                        rs.getInt("status"),
                        rs.getString("body")
                );
                listSubject.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listSubject;
    }

    public SubjectDTO getSubjectDetail(String id) {
        try ( Connection connection = DBConnect.getConnection()) {
            List<UserDTO> a = getListUser();
            String sql = "SELECT * FROM swp_project.subject where subject_id =  " + id;
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                SubjectDTO s = new SubjectDTO(
                        rs.getInt("subject_id"),
                        rs.getString("subject_code"),
                        rs.getString("subject_name"),
                        checkExit(rs.getInt("manager_id"), a),
                        checkExit(rs.getInt("expert_id"), a),
                        rs.getInt("status"),
                        rs.getString("body")
                );
                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String checkExit(int id, List<UserDTO> a) {
        for (UserDTO userDTO : a) {
            if (userDTO.getUser_id() == id) {
                return userDTO.getFullname();
            }
        }
        return null;
    }

    public UserDTO getUser(int user_id) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "select user_id,full_name from user where user_id = " + user_id;
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                UserDTO u = new UserDTO(rs.getInt("user_id"), rs.getString("full_name"));
                return u;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<UserDTO> getListUser() {
        try ( Connection connection = DBConnect.getConnection()) {
            List<UserDTO> list = new ArrayList<>();
            String sql = "select user_id,full_name from user  ";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                UserDTO u = new UserDTO(rs.getInt("user_id"), rs.getString("full_name"));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<UserDTO> getListUser1(int role) {
        try ( Connection connection = DBConnect.getConnection()) {
            List<UserDTO> list = new ArrayList<>();
            String sql = "select u.user_id,u.full_name from user_role r join user u on r.user_id = u.user_id  where r.role_id = " + role;
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                UserDTO u = new UserDTO(rs.getInt("user_id"), rs.getString("full_name"));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String createOrderBy(String order, String sort) {
        String colOrder = "";
        String orderBy = " ";
        if (!(order != null && sort != null)) {
            return orderBy;
        }
        switch (order) {
            case "Id":
                colOrder = "subject_id";
                break;
            case "code":
                colOrder = "subject_code";
                break;
            case "name":
                colOrder = "subject_name";
                break;
            case "manager":
                colOrder = "manager";
                break;
            case "expert":
                colOrder = "expert";
                break;
            case "status":
                colOrder = "status";
                break;

        }
        if (colOrder != null) {
            orderBy = " order by " + colOrder + " " + sort.toLowerCase();
        }
        return orderBy;
    }

    public int totalPage() throws SQLException {
        Connection connection = DBConnect.getConnection();
        String sql = "select count(*) from subject";
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            return rs.getInt(1);
        }
        return 0;
    }

    public void updateStatus(int subject_id, int status) throws SQLException {
        Connection connection = DBConnect.getConnection();
        String sql = "UPDATE subject SET status = " + status + " WHERE subject_id = " + subject_id;
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.executeUpdate();
    }

    public void editSubject(int subject_id, String code, String name, String manager, String expert, int staus, String des) throws SQLException {
        Connection connection = DBConnect.getConnection();
        String sql = "UPDATE subject"
                + " SET subject_code = \"" + code + "\",subject_name = \"" + name + "\","
                + "manager_id = " + manager + ",expert_id = \"" + expert + "\", status = " + staus + ",body = \"" + des + "\" \n"
                + " WHERE subject_id = " + subject_id;
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.executeUpdate();
    }

    public void addSubject(String subjectcode,String subjectName,int manager,int expert, int status, String body) throws SQLException{
        Connection connection = DBConnect.getConnection();
        String sql = "INSERT INTO subject"
                + " (subject_code, subject_name, manager_id, expert_id, status, body)"
                + " VALUES ('"+subjectcode+"', '"+subjectName+"', '"+manager+"', '"+expert+"', '"+status+"', '"+body+"');";
               
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.executeUpdate();
    }
        public static ArrayList<Subject> getAllSubject() {
        ArrayList<Subject> List = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT * FROM subject";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Subject s = new Subject(
                        rs.getInt("subject_id"),
                        rs.getString("subject_code"),
                        rs.getString("subject_name"),
                        rs.getInt("manager_id"),
                        rs.getInt("expert_id"),
                        rs.getInt("status"),
                        rs.getString("body"));
                List.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return List;
        
    }

    public static void main(String[] args) throws SQLException {
        SubjectDAO dao = new SubjectDAO();
        dao.addSubject("PMG202c", "Project management", 1, 4, 1, "bsdf");
    }
}
