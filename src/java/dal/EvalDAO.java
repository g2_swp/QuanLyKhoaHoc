/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Assignment;
import model.Eval_criteria;

/**
 *
 * @author MEP
 */
public class EvalDAO {
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    
    public List<Eval_criteria> getAllEval() {
        List<Eval_criteria> list = new ArrayList<>();
        String query = "SELECT * FROM eval_criteria";
        try {
            conn = DBConnect.getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                Eval_criteria e = new Eval_criteria();
                e.setCriteria_id(rs.getInt(1));
                Assignment a = getAssignmentById(rs.getInt(2));
                e.setAssignment(a);
                e.setMilestone_id(rs.getInt(3));
                e.setCriteria_name(rs.getString(4));
                e.setIs_team_eval(rs.getString(5));
                e.setEval_weight(rs.getString(6));
                e.setMax_loc(rs.getInt(7));
                e.setStatus(rs.getInt(8));
                e.setDescription(rs.getString(9));
                list.add(e);
            }
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public Eval_criteria getEvalById(int contact_id) {
        String sql = "SELECT * FROM eval_criteria WHERE criteria_id=? ";
        try ( Connection connection = DBConnect.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, contact_id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Eval_criteria e = new Eval_criteria(
                        rs.getInt(1),
                        getAssignmentById(rs.getInt(2)),
                        rs.getInt(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getInt(7),
                    rs.getInt(8),
                        rs.getString(9)
                );
                return e;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void editEval(int criteria_id, String criteria_name, String is_team_eval, 
            String eval_weight, int max_loc, int status, String description) {
        String query = "UPDATE eval_criteria SET criteria_name = ?, is_team_eval = ?, \n" +
"            eval_weight = ?, max_loc = ?, status = ?, description = ? WHERE criteria_id = ? ";
        try {
            conn = DBConnect.getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1,criteria_name);
            ps.setString(2,is_team_eval);
            ps.setString(3, eval_weight);
            ps.setInt(4, max_loc);
            ps.setInt(5, status);
            ps.setString(6, description);
            ps.setInt(7, criteria_id);
            ps.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Eval_criteria> getListByPage(ArrayList<Eval_criteria> list, int start, int end) {
        ArrayList<Eval_criteria> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }
    
    public List<Assignment> getAssTitle() {
        List<Assignment> list = new ArrayList<>();
        String query = "SELECT ass_id, title FROM assignment";
        try {
            conn = DBConnect.getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Assignment(
                        rs.getInt(1),
                        rs.getString(2)
                ));
            }
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public Assignment getAssignmentById(int ass_id) {
        String query = "SELECT ass_id, title FROM assignment WHERE ass_id=?";
        try ( Connection connection = DBConnect.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, ass_id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Assignment s = new Assignment(
                        rs.getInt(1),
                        rs.getString(2));
                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Eval_criteria> FilterEvalByAssignment(String ass_id) {
        List<Eval_criteria> list = new ArrayList<>();
        String query = "SELECT * FROM eval_criteria";
        
        if (ass_id == null || ass_id.equals("")) {
        } else {
            query += " WHERE ass_id=? ";
        }
        
        try {
            conn = DBConnect.getConnection();
            ps = conn.prepareStatement(query);
            if (ass_id == null || ass_id.equals("")) {
            } else {
                ps.setString(1, ass_id);
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                Eval_criteria e = new Eval_criteria();
                e.setCriteria_id(rs.getInt(1));
                Assignment a = getAssignmentById(rs.getInt(2));
                e.setAssignment(a);
                e.setMilestone_id(rs.getInt(3));
                e.setCriteria_name(rs.getString(4));
                e.setIs_team_eval(rs.getString(5));
                e.setEval_weight(rs.getString(6));
                e.setMax_loc(rs.getInt(7));
                e.setStatus(rs.getInt(8));
                e.setDescription(rs.getString(9));
                list.add(e);
            }
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
}
