/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Setting;

/**
 *
 * @author Hung
 */
public class SettingDAO {

    public ArrayList<Setting> getAll() {
        ArrayList<Setting> List = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT * FROM setting";
            
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Setting s = new Setting(
                        rs.getInt("setting_id"),
                        rs.getInt("type_id"),
                        rs.getString("setting_title"),
                        rs.getString("setting_value"),
                        rs.getInt("display_order"),
                        rs.getInt("status"),
                        rs.getString("description")
                );
                List.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return List;
    }

    public static int countSettingsNumber() {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT COUNT(*) as count FROM setting;";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                int s = rs.getInt("count");
                return s;
            }

        } catch (SQLException ex) {
            Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public ArrayList<Setting> getSomeSettingList(int start, String sort_column) {
        ArrayList<Setting> List = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "select s.setting_id, s.type_id, s.setting_title,s.setting_value,s.display_order,s.description,s.status,s2.setting_title as type_title from setting s left join setting s2 on s.type_id=s2.setting_id order by s." + sort_column + " asc LIMIT " + start + ", 10";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Setting s = new Setting(
                        rs.getInt("setting_id"),
                        rs.getInt("type_id"),
                        rs.getString("setting_title"),
                        rs.getString("setting_value"),
                        rs.getInt("display_order"),
                        rs.getInt("status"),
                        rs.getString("description"),
                        rs.getString("type_title")
                );
                List.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return List;
    }

    public Setting getSetting(int setting_id) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT * FROM setting where setting_id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, setting_id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Setting s = new Setting(
                        rs.getInt("setting_id"),
                        rs.getInt("type_id"),
                        rs.getString("setting_title"),
                        rs.getString("setting_value"),
                        rs.getInt("display_order"),
                        rs.getInt("status"),
                        rs.getString("description")
                );
                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void editSetting(Setting s) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "UPDATE setting SET type_id = ?, setting_title = ?, setting_value = ?, display_order=?,status=?,description=? WHERE setting_id = ?;";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, s.getType_id());
            statement.setString(2, s.getSetting_title());
            statement.setString(3, s.getSetting_value());
            statement.setInt(4, s.getDisplay_order());
            statement.setInt(5, s.getStatus());
            statement.setString(6, s.getDescription());
            statement.setInt(7, s.getSetting_id());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Setting> getCategory() {
        ArrayList<Setting> List = new ArrayList<>();
        String sql = "SELECT * FROM setting";
        try ( Connection connection = DBConnect.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Setting s = new Setting(
                        rs.getInt(1),
                        rs.getString(3)
                );
                List.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return List;
    }

    public ArrayList<Setting> getAllSettingType() {
        ArrayList<Setting> List = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "select s.setting_id, s.type_id, s.setting_title,s.setting_value,s.display_order,"
                    + "s.description,s.status,s2.setting_title as type_title from "
                    + "setting s left join setting s2 on s.type_id=s2.setting_id where s.setting_id<=10";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Setting s = new Setting(
                        rs.getInt("setting_id"),
                        rs.getInt("type_id"),
                        rs.getString("setting_title"),
                        rs.getString("setting_value"),
                        rs.getInt("display_order"),
                        rs.getInt("status"),
                        rs.getString("description"),
                        rs.getString("type_title")
                );
                List.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return List;
    }

    public ArrayList<Setting> getAllUserRoleSetting() {
        ArrayList<Setting> List = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT * FROM setting where type_id = 1";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Setting s = new Setting(
                        rs.getInt("setting_id"),
                        rs.getInt("type_id"),
                        rs.getString("setting_title"),
                        rs.getString("setting_value"),
                        rs.getInt("display_order"),
                        rs.getInt("status"),
                        rs.getString("description")
                );
                List.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return List;
    }

    public ArrayList<Setting> getAllScreenSetting() {
        ArrayList<Setting> List = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT * FROM setting where type_id = 5";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Setting s = new Setting(
                        rs.getInt("setting_id"),
                        rs.getInt("type_id"),
                        rs.getString("setting_title"),
                        rs.getString("setting_value"),
                        rs.getInt("display_order"),
                        rs.getInt("status"),
                        rs.getString("description")
                );
                List.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return List;
    }

    public ArrayList<Setting> searchAndFilter(String sql_query) {
        ArrayList<Setting> List = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = sql_query;
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Setting s = new Setting(
                        rs.getInt("setting_id"),
                        rs.getInt("type_id"),
                        rs.getString("setting_title"),
                        rs.getString("setting_value"),
                        rs.getInt("display_order"),
                        rs.getInt("status"),
                        rs.getString("description")
                );
                List.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return List;
    }

    public int max_id() {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT max(setting_id) as max from setting";

            PreparedStatement statement = connection.prepareStatement(sql);

            ResultSet rs = statement.executeQuery();
            int s = 0;
            if (rs.next()) {
                s = rs.getInt("max");
                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public static void addNewSetting(Setting s) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "insert into setting (setting_id, type_id, setting_title, setting_value, display_order, status, description)\n"
                    + "values(?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, s.getSetting_id());
            statement.setInt(2, s.getType_id());
            statement.setString(3, s.getSetting_title());
            statement.setString(4, s.getSetting_value());
            statement.setInt(5, s.getDisplay_order());
            statement.setInt(6, s.getStatus());
            statement.setString(7, s.getDescription());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void deleteSetting(int setting_id) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "delete FROM setting where setting_id=?;";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, setting_id);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
