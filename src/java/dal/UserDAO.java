/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import utilities.validate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author Hung
 */
public class UserDAO {

    public ArrayList<User> getAll() {
        ArrayList<User> bookList = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT * FROM user";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User s = new User(
                        rs.getInt("user_id"),
                        rs.getString("user_name"),
                        rs.getString("full_name"),
                        rs.getString("email"),
                        rs.getString("mobile"),
                        rs.getString("password"),
                        rs.getString("avatar_url"),
                        rs.getString("status"),
                        rs.getString("note"));
                bookList.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bookList;
    }

    // update Password
    public void updatePasswordByToken(String password, String token) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "UPDATE user SET password = ? WHERE status = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, password);
            statement.setString(2, token);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //updatepassword by email

    public void updatePasswordByEmail(String password, String email) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "UPDATE user SET password = ? WHERE email = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, password);
            statement.setString(2, email);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //status = token? Khong hieu cach dung status cho lam, Dung de luu tam token
    public void saveToken(String email, String token) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "UPDATE user SET status=? where email=?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, token);
            statement.setString(2, email);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public User findUserByToken(String token) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT * FROM user u where u.status= ?";
            //UPDATE users SET password = ? WHERE email = ?
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, token);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                User s = new User(
                        rs.getInt("user_id"),
                        rs.getString("user_name"),
                        rs.getString("full_name"),
                        rs.getString("email"),
                        rs.getString("mobile"),
                        rs.getString("password"),
                        rs.getString("avatar_url"),
                        rs.getString("status"),
                        rs.getString("note")
                );
                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public User findByUserId(String user_id) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT * FROM user u where u.user_id=?";
            //UPDATE users SET password = ? WHERE email = ?
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, user_id);

            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                User s = new User(
                        rs.getInt("user_id"),
                        rs.getString("user_name"),
                        rs.getString("full_name"),
                        rs.getString("email"),
                        rs.getString("mobile"),
                        rs.getString("password"),
                        rs.getString("avatar_url"),
                        rs.getString("status"),
                        rs.getString("note")
                );
                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public User findByUserNameAndEmail(String user_Name, String email) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT * FROM user u where u.user_name=? and u.email= ?";
            //UPDATE users SET password = ? WHERE email = ?
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, user_Name);
            statement.setString(2, email);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                User s = new User(
                        rs.getInt("user_id"),
                        rs.getString("user_name"),
                        rs.getString("full_name"),
                        rs.getString("email"),
                        rs.getString("mobile"),
                        rs.getString("password"),
                        rs.getString("avatar_url"),
                        rs.getString("status"),
                        rs.getString("note")
                );
                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);

        }
        return null;
    }

    public static void main(String[] args) {
        UserDAO u = new UserDAO();
        validate val = new validate();
        ArrayList<User> a = u.getAll();
        for (User user : a) {
            System.out.println(user);
        }
        System.out.println(val.getMd5("xyz"));
        System.out.println(u.findByUserId("5").getEmail());
        System.out.println(val.getMd5("kwang123").equals("862edad6e6abd2892b168d8f565f4cd8"));
    }

    public static void addNewUser(User s) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "INSERT INTO user (user_id, user_name, full_name, email, mobile, password, avatar_url, status, note) values (?,?,?,?,?,?,?,?,?);";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, s.getUser_id());
            statement.setString(2, s.getUser_name());
            statement.setString(3, s.getFull_name());
            statement.setString(4, s.getEmail());
            statement.setString(5, s.getMobile());
            statement.setString(6, s.getPassword());
            statement.setString(7, s.getAvatar_url());
            statement.setString(8, s.getStatus());
            statement.setString(9, s.getNote());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public User findByUserEmail(String email) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT * FROM user where email=?";

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, email);

            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                User s = new User(
                        rs.getInt("user_id"),
                        rs.getString("user_name"),
                        rs.getString("full_name"),
                        rs.getString("email"),
                        rs.getString("mobile"),
                        rs.getString("password"),
                        rs.getString("avatar_url"),
                        rs.getString("status"),
                        rs.getString("note")
                );
                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int max_id() {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT max(user_id) as max from user";

            PreparedStatement statement = connection.prepareStatement(sql);

            ResultSet rs = statement.executeQuery();
            int s = 0;
            if (rs.next()) {
                s = rs.getInt("max");
                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public void insertNew(User u) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "insert into user (user_id, user_name, full_name, email, mobile, password, avatar_url, status,note)\n"
                    + "values(?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, u.getUser_id());
            statement.setString(2, u.getUser_name());
            statement.setString(3, u.getFull_name());
            statement.setString(4, u.getEmail());
            statement.setString(5, u.getMobile());
            statement.setString(6, u.getPassword());
            statement.setString(7, u.getAvatar_url());
            statement.setString(8, u.getStatus());
            statement.setString(9, u.getNote());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
