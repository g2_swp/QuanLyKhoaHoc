/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Class_Model;
import model.Subject;

/**
 *
 * @author MEP
 */
public class ClassDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public List<Class_Model> getAllClass() {
        List<Class_Model> list = new ArrayList<>();
        String query = "SELECT * FROM class";
        try {
            conn = DBConnect.getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Class_Model(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getString(8)
                ));
            }
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public Class_Model getClassById(int class_id) {
        String sql = "SELECT * FROM class WHERE class_id=? ";
        try ( Connection connection = DBConnect.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, class_id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Class_Model c = new Class_Model(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getString(8)
                );
                return c;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void addClass(int subject_id, String class_code, int trainer_id,
            int supporter_id, int team_id, int status, String description) {
        String query = "INSERT INTO class (subject_id, class_code, trainer_id, supporter_id,team_id, status, description)\n"
                + "VALUES (?, ?, ?, ? , ? , ?, ?)";
        try {
            conn = DBConnect.getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1,subject_id);
            ps.setString(2,class_code);
            ps.setInt(3,trainer_id);
            ps.setInt(4, supporter_id);
            ps.setInt(5, team_id);
            ps.setInt(6, status);
            ps.setString(7,description);
            ps.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editClass(int class_id, int subject_id, int trainer_id,
            int supporter_id, int team_id, int status, String description) {
        String query = "UPDATE class SET subject_id = ?, trainer_id = ?,\n" +
"            supporter_id = ?, team_id = ?, status = ?, description = ? WHERE class_id = ? ";
        try {
            conn = DBConnect.getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1,subject_id);
            ps.setInt(2,trainer_id);
            ps.setInt(3, supporter_id);
            ps.setInt(4, team_id);
            ps.setInt(5, status);
            ps.setString(6, description);
            ps.setInt(7,class_id);
            ps.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void DeteleClass(int class_id) {
        String query = "DELETE FROM class WHERE class_id=?";
        try {
            conn = DBConnect.getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, class_id);
            ps.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Class_Model> getListByPage(ArrayList<Class_Model> list, int start, int end) {
        ArrayList<Class_Model> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }
    
    public List<Subject> getSubject_Code() {
        List<Subject> list = new ArrayList<>();
        String query = "SELECT subject_id, subject_code FROM subject;";
        try {
            conn = DBConnect.getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Subject(
                        rs.getInt(1),
                        rs.getString(2)
                ));
            }
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<Class_Model> FilterClassBySub(String subject_id) {
        List<Class_Model> list = new ArrayList<>();
        String query = "SELECT * FROM class";
        
        if (subject_id == null || subject_id.equals("")) {
        } else {
            query += " WHERE subject_id=? ";
        }
        
        try {
            conn = DBConnect.getConnection();
            ps = conn.prepareStatement(query);
            if (subject_id == null || subject_id.equals("")) {
            } else {
                ps.setString(1, subject_id);
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Class_Model(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getString(8)
                ));
            }
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
