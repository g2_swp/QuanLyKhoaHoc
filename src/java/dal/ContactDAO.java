/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Contact;
import model.Setting;

/**
 *
 * @author MEP
 */
public class ContactDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public void addContact(int category_id, String full_name, String email, String mobile, String message) {
        String query = "INSERT INTO web_contact (category_id, full_name, email, mobile, message, status)\n"
                + "VALUES (?, ?, ?, ? , ? , 0)";
        try {
            conn = DBConnect.getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, category_id);
            ps.setString(2, full_name);
            ps.setString(3, email);
            ps.setString(4, mobile);
            ps.setString(5, message);
            ps.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Contact> getAllContact() {
        List<Contact> list = new ArrayList<>();
        String query = "SELECT * FROM web_contact";
        try {
            conn = DBConnect.getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                Contact c = new Contact();
                c.setContact_id(rs.getInt(1));
                Setting s = getCategoryById(rs.getInt(2));
                c.setSetting(s);
                c.setStaff_id(rs.getInt(3));
                c.setFull_name(rs.getString(4));
                c.setEmail(rs.getString(5));
                c.setMobile(rs.getString(6));
                c.setMessage(rs.getString(7));
                c.setResponse(rs.getString(8));
                c.setStatus(rs.getInt(9));
                list.add(c);
            }
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    //https://youtu.be/OsCMLOfJQH0
    public List<Contact> getListByPage(ArrayList<Contact> list, int start, int end) {
        ArrayList<Contact> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public void editContact(int category_id, int staff_id, String response) {
        String query = "UPDATE web_contact SET staff_id = ?, response= ?, status = 1 WHERE (contact_id = ? )";
        try {
            conn = DBConnect.getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, staff_id);
            ps.setString(2, response);
            ps.setInt(3, category_id);
            ps.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Setting getCategoryById(int category_id) {
        String sql = "SELECT * FROM setting WHERE setting_id=? ";
        try ( Connection connection = DBConnect.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, category_id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Setting s = new Setting(
                        rs.getInt(1),
                        rs.getString(3));
                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Contact> getContactByAny(String category_id,String searchName) {
        List<Contact> list = new ArrayList<>();
        String query = "SELECT * FROM web_contact";
        
        if (category_id == null || category_id.equals("")) {
        } else {
            query += " WHERE category_id=? ";
        }
        
        if (searchName == null || searchName.equals("")) {
        } else if (query.indexOf("WHERE")!=-1){
            query += " and full_name like '%" + searchName +"%'";
        } else {
            query += " WHERE full_name like '%" + searchName +"%' ";
        }
        
        try {
            conn = DBConnect.getConnection();
            ps = conn.prepareStatement(query);
            if (category_id == null || category_id.equals("")) {
            } else {
                ps.setString(1, category_id);
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                Contact c = new Contact();
                c.setContact_id(rs.getInt(1));
                Setting s = getCategoryById(rs.getInt(2));
                c.setSetting(s);
                c.setStaff_id(rs.getInt(3));
                c.setFull_name(rs.getString(4));
                c.setEmail(rs.getString(5));
                c.setMobile(rs.getString(6));
                c.setMessage(rs.getString(7));
                c.setResponse(rs.getString(8));
                c.setStatus(rs.getInt(9));
                list.add(c);
            }
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    public Contact getContactById(int contact_id) {
        String sql = "SELECT * FROM web_contact WHERE contact_id=? ";
        try ( Connection connection = DBConnect.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, contact_id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Contact c = new Contact();
                c.setContact_id(rs.getInt(1));
                Setting s = getCategoryById(rs.getInt(2));
                c.setSetting(s);
                c.setStaff_id(rs.getInt(3));
                c.setFull_name(rs.getString(4));
                c.setEmail(rs.getString(5));
                c.setMobile(rs.getString(6));
                c.setMessage(rs.getString(7));
                c.setResponse(rs.getString(8));
                c.setStatus(rs.getInt(9));
                return c;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
