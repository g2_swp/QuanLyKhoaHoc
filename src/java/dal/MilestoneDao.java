/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import dto.Assignment2DTO;
import dto.AssignmentDTO;
import dto.ClassDTO;
import dto.MileStoneDTO;
import dto.Milestone2DTO;
import dto.Subject2DTO;
import dto.SubjectDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Assignment;
import model.Milestone;

/**
 *
 * @author luong
 */
public class MilestoneDao {

    public ArrayList<MileStoneDTO> getListMilestone(int index) {
        ArrayList<MileStoneDTO> List = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT m.milestone_id,a.title,c.class_code,m.from_date,m.to_date,m.title,m.ass_body,m.description,m.status\n"
                    + " FROM swp_project.milestone m join assignment a on m.ass_id = a.ass_id\n"
                    + " join class c on c.class_id = m.class_id" + " LIMIT " + (index * 5 - 5) + ", 5";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                MileStoneDTO s = new MileStoneDTO(
                        rs.getInt("m.milestone_id"),
                        rs.getString("a.title"),
                        rs.getString("c.class_code"),
                        rs.getString("m.from_date"),
                        rs.getString("m.to_date"),
                        rs.getString("m.title"),
                        rs.getString("m.ass_body"),
                        rs.getString("m.description"),
                        rs.getInt("m.status")
                );
                List.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return List;
    }

    public ArrayList<MileStoneDTO> getSearchListMilestone(int index, String class_code, String title, String fromdate, String todate, String status) {
        ArrayList<MileStoneDTO> List = new ArrayList<>();
        StringBuilder condition = new StringBuilder();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT m.milestone_id,a.title,c.class_code,m.from_date,m.to_date,m.title,m.ass_body,m.description,m.status\n"
                    + " FROM swp_project.milestone m join assignment a on m.ass_id = a.ass_id\n"
                    + " join class c on c.class_id = m.class_id [CONDITION]" + " LIMIT " + (index * 5 - 5) + ", 5";

            if (title != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                condition.append("(a.title) like '%" + title + "%' ");
            }
            if (class_code != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                condition.append("c.class_code = \"" + class_code + "\" ");
            }
            if (fromdate != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                condition.append("m.from_date >= '" + todate + "'");
            }
            if (todate != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                condition.append("m.to_date <= '" + todate + "'");
            }
            if (status != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                condition.append("m.status = " + status + "");
            }

            if (condition.length() != 0) {
                condition.insert(0, " where ");
            }
            sql = sql.replace("[CONDITION]", condition.toString());
            PreparedStatement statement = connection.prepareStatement(sql.toString());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                MileStoneDTO s = new MileStoneDTO(
                        rs.getInt("m.milestone_id"),
                        rs.getString("a.title"),
                        rs.getString("c.class_code"),
                        rs.getString("m.from_date"),
                        rs.getString("m.to_date"),
                        rs.getString("m.title"),
                        rs.getString("m.ass_body"),
                        rs.getString("m.description"),
                        rs.getInt("m.status")
                );
                List.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return List;
    }

    public MileStoneDTO getMilestone(int classid, int assid) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT m.milestone_id,a.title,c.class_code,m.from_date,m.to_date,m.title,a.ass_body,m.description,m.status\n"
                    + "                     FROM swp_project.milestone m join assignment a on m.ass_id = a.ass_id\n"
                    + "                     join class c on c.class_id = m.class_id where m.ass_id = " + assid + " and m.class_id = " + classid;
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                MileStoneDTO s = new MileStoneDTO(
                        rs.getInt("m.milestone_id"),
                        rs.getString("a.title"),
                        rs.getString("c.class_code"),
                        rs.getString("m.from_date"),
                        rs.getString("m.to_date"),
                        rs.getString("m.title"),
                        rs.getString("m.ass_body"),
                        rs.getString("m.description"),
                        rs.getInt("m.status")
                );
                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Milestone2DTO getMilestone1(int milestoneid) {
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT m.milestone_id,a.ass_id,a.title,c.class_code,c.class_id,m.from_date,m.to_date,m.title,a.ass_body,m.description,m.status\n"
                    + "                                      FROM swp_project.milestone m join assignment a on m.ass_id = a.ass_id\n"
                    + "                               join class c on c.class_id = m.class_id where m.milestone_id = " + milestoneid;
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Milestone2DTO s = new Milestone2DTO(
                        rs.getInt("m.milestone_id"),
                        rs.getString("a.title"),
                        rs.getInt("a.ass_id"),
                        rs.getString("c.class_code"),
                        rs.getInt("c.class_id"),
                        rs.getString("m.from_date"),
                        rs.getString("m.to_date"),
                        rs.getString("m.title"),
                        rs.getString("a.ass_body"),
                        rs.getString("m.description"),
                        rs.getInt("m.status")
                );
                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<MileStoneDTO> getSearchListMilestone(int class_id) {
        ArrayList<MileStoneDTO> List = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT m.milestone_id,a.title,c.class_code,m.from_date,m.to_date,m.title,m.ass_body,m.description,m.status\n"
                    + " FROM swp_project.milestone m join assignment a on m.ass_id = a.ass_id\n"
                    + " join class c on c.class_id = m.class_id";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                MileStoneDTO s = new MileStoneDTO(
                        rs.getInt("m.milestone_id"),
                        rs.getString("a.title"),
                        rs.getString("c.class_code"),
                        rs.getString("m.from_date"),
                        rs.getString("m.to_date"),
                        rs.getString("m.title"),
                        rs.getString("m.ass_body"),
                        rs.getString("m.description"),
                        rs.getInt("m.status")
                );
                List.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return List;
    }

    public MileStoneDTO getMilestoneDetail(String milestone_id) {

        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT m.milestone_id,a.title,c.class_code,m.from_date,m.to_date,m.title,a.ass_body,m.description,m.status\n"
                    + "                     FROM swp_project.milestone m join assignment a on m.ass_id = a.ass_id\n"
                    + "                     join class c on c.class_id = m.class_id where m.milestone_id = 1";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                MileStoneDTO s = new MileStoneDTO(
                        rs.getInt("m.milestone_id"),
                        rs.getString("a.title"),
                        rs.getString("c.class_code"),
                        rs.getString("m.from_date"),
                        rs.getString("m.to_date"),
                        rs.getString("m.title"),
                        rs.getString("a.ass_body"),
                        rs.getString("m.description"),
                        rs.getInt("m.status")
                );
                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<AssignmentDTO> getListAss() {
        List<AssignmentDTO> list = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "select a.ass_id,s.subject_name,a.title,a.ass_body,a.eval_weight,"
                    + "a.is_ongoing,a.is_team_work,a.status,a.subject_id\n"
                    + " from assignment a join subject s on a.subject_id = s.subject_id where a.status = 1";

            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                AssignmentDTO s = new AssignmentDTO(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9)
                );
                list.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }

    public int addNewmilestone(int ass_id, int class_id, String fromdate, String todate, String title, String ass_body, String des, int status) throws SQLException {
        int n = 0;
        Connection connection = DBConnect.getConnection();
        String sql = "INSERT INTO swp_project.milestone (ass_id, class_id, from_date, to_date, title, ass_body, description, status)"
                + " VALUES ( '" + ass_id + "', '" + class_id + "', '" + fromdate + "', '" + todate + "', '" + title + "', '" + ass_body + "', '" + des + "', '" + status + "');";
        PreparedStatement statement = connection.prepareStatement(sql);
        n = statement.executeUpdate();
        return n;
    }

    public int editMilestone(int milestoneid, String fromdate, String todate, String title, String ass_body, String des, int status) throws SQLException {
        int n = 0;
        Connection connection = DBConnect.getConnection();
        String sql = "Update milestone SET from_date = '" + fromdate + "', to_date = '" + todate + "', "
                + " title = '" + title + "',ass_body = '" + ass_body + "',description = '" + des + "',status = " + status + "\n"
                + "WHERE milestone_id = " + milestoneid;
        PreparedStatement statement = connection.prepareStatement(sql);
        n = statement.executeUpdate();
        return n;
    }

    public int milestoneId(int ass_id, int class_id) throws SQLException {
        int n = 0;
        Connection connection = DBConnect.getConnection();
        String sql = "SELECT milestone_id FROM swp_project.milestone where ass_id = " + ass_id + " and class_id = " + class_id;
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            return rs.getInt(1);
        }

        return 0;
    }

    public int classId(int subject_id, String class_code) throws SQLException {
        int n = 0;
        Connection connection = DBConnect.getConnection();
        String sql = "SELECT class_id FROM swp_project.class where class_code = '" + class_code + "' and subject_id = " + subject_id;
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            return rs.getInt(1);
        }

        return 0;
    }

    public void addeVal_criteria(int ass_id, int milestone_id) throws SQLException {
        Connection connection = DBConnect.getConnection();
        String sql = "UPDATE eval_criteria SET milestone_id = " + milestone_id + " WHERE ass_id = " + ass_id;
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.executeUpdate();
    }
     public void editmilestone(int milestone_id,int status) throws SQLException {
        Connection connection = DBConnect.getConnection();
        String sql = "update milestone set status = "+status+" where milestone_id = "+milestone_id;
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.executeUpdate();
    }

    public List<ClassDTO> getListC() {
        List<ClassDTO> list = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT distinct(class_code) FROM swp_project.class where status = 1";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                ClassDTO s = new ClassDTO(
                        rs.getString(1)
                );
                list.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }

    public List<Subject2DTO> getListSubject(String classcode) {
        List<Subject2DTO> list = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "SELECT s.subject_id,s.subject_code FROM swp_project.subject s join class c on s.subject_id = c.subject_id\n"
                    + "where c.class_code = '" + classcode + "'";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Subject2DTO s = new Subject2DTO(rs.getInt(1), rs.getString(2));
                list.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }

    public List<Assignment2DTO> getListAss(int subjectid) {
        List<Assignment2DTO> list = new ArrayList<>();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = "select distinct(a.ass_id),a.title from assignment a join subject s on a.subject_id = s.subject_id where s.subject_id = " + subjectid;
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Assignment2DTO s = new Assignment2DTO(rs.getInt(1), rs.getString(2));
                list.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }

    public int getTotalP(String class_code, String title, String fromdate, String todate, String status) {
        StringBuilder condition = new StringBuilder();
        try ( Connection connection = DBConnect.getConnection()) {
            String sql = " SELECT count(m.milestone_id) FROM swp_project.milestone m join assignment a on m.ass_id = a.ass_id\n"
                    + "                    join class c on c.class_id = m.class_id  [CONDITION]";

            if (title != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                condition.append("(a.title) like '%" + title + "%' ");
            }
            if (class_code != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                condition.append("c.class_code = \"" + class_code + "\" ");
            }
            if (fromdate != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                condition.append("m.from_date >= '" + todate + "'");
            }
            if (todate != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                condition.append("m.to_date <= '" + todate + "'");
            }
            if (status != null) {
                if (condition.length() != 0) {
                    condition.append(" and ");
                }
                condition.append("status = \"" + status + "\"");
            }

            if (condition.length() != 0) {
                condition.insert(0, " where ");
            }
            sql = sql.replace("[CONDITION]", condition.toString());
            PreparedStatement statement = connection.prepareStatement(sql.toString());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
              return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public static void main(String[] args) {
        MilestoneDao dao = new MilestoneDao();
        ArrayList<MileStoneDTO> d = dao.getSearchListMilestone(1, "SE1501", null, null, null, "0");
        for (MileStoneDTO mileStoneDTO : d) {
            System.out.println(mileStoneDTO);
        }
    }
}
