/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import dal.UserDAO;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import model.User;

/**
 *
 * @author kieuq
 */
public class validate {

    public validate() {
    }
    
     public String getMd5(String input) 
    { 
        try { 
            // Static getInstance method is called with hashing MD5 
            MessageDigest md = MessageDigest.getInstance("MD5"); 
  
            // digest() method is called to calculate message digest 
            //  of an input digest() return array of byte 
            byte[] messageDigest = md.digest(input.getBytes()); 
  
            // Convert byte array into signum representation 
            BigInteger no = new BigInteger(1, messageDigest); 
  
            // Convert message digest into hex value 
            String hashtext = no.toString(16); 
            while (hashtext.length() < 32) { 
                hashtext = "0" + hashtext; 
            } 
            return hashtext; 
        }  
        // For specifying wrong message digest algorithms 
        catch (NoSuchAlgorithmException e) { 
            throw new RuntimeException(e); 
        } 
    }
     
     public boolean verify(String password_in_Md5, String password_default){
         return password_in_Md5.equals(password_default);
     }
     public static void main(String[] args) throws NoSuchAlgorithmException {
         UserDAO dao=new UserDAO();
         User u=dao.findByUserNameAndEmail("maimm", "mimhmm@gmail.com");
         //dao.updatePasswordByEmail(getMd5("hello"), u.getEmail());
         System.out.println(u.getPassword());
//         System.out.println(getMd5("hello"));
//         System.out.println(verify(getMd5("hello"), u.getPassword()));
         
    }
}
