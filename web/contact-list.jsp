<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Contact List</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="../../../index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="course/images/logo.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <!-- Table Sort Css -->
        <link href="assets/css/tablesort.css" rel="stylesheet" type="text/css" />
        
        <style>
            i.fa-regular {
                font-family: fontawesome !important;
            }
            .logo span {
                font-family: 'Open Sans', sans-serif;
                font-size: 30px;
                font-weight: 900;
                vertical-align: middle;
                text-transform: uppercase;
                margin-left: 3px;
            }
            .table-title .btn span {
                float: left;
            }
            .table-filter .filter-group {
                float: right;
                margin-left: 15px;
            }

            .table-filter label {
                font-size: 18px;
                margin-left: 10px;
            }
            .table-filter select, .table-filter input {
                display: inline-block;
                margin-left: 5px;
            }
            .table-filter input {
                width: 200px;
                display: inline-block;
            }
            .filter-group select.form-control, .filter-group select.form-select{
                width: 145px;
            }
            .filter-icon {
                float: right;
            }
        </style>

    </head>

    <body class="bg-dark">
        
        <div class="page-wrapper doctris-theme">
            <!-- SideBar -->
            <jsp:include page="AdSideBar.jsp"></jsp:include>
                <!-- SideBar  -->

                <!-- Start Page Content -->
                <main class="page-content bg-light">

                    <!-- Header  -->
                <jsp:include page="AdHeader.jsp"></jsp:include>
                    <!-- Header  -->

                    <!-- container -->
                    <div class="container-fluid">
                        <div class="layout-specing">
                            <div class="row bg-warning text-white p-4">
                                <div class="col-md-3 ps-4">
                                    <h2 class="mb-0"><b class="text-dark">Contact</b> List</h2>
                                </div>
                                <div class="col-md-9 mt-2">
                                    <form action="ContactList" method="Post">
                                        <div class="table-filter">
                                            <button type="submit" class="btn btn-icon btn-pills btn-info float-end ms-3"><i class="uil uil-search"></i></button>
                                            <div class="filter-group">
                                                <label>Search</label>
                                                <input name="searchName" value="${findName}" type="text" class="form-control">
                                            </div>
                                            <div class="filter-group">
                                                <label>Category</label>
                                                <select class="form-select" name="category_id">
                                                    <option value="">All Category</option>
                                                <c:forEach items="${listCate}" var="c">
                                                    <option value="${c.setting_id}" ${c.setting_id == CateID?"selected":""}>${c.setting_title}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <span class="filter-icon text-dark mt-2"><i class="fa fa-filter"></i></span>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive shadow rounded">
                                    <table class="table table-sortable table-center bg-white mb-0">
                                        <thead>
                                            <tr>
                                                <th class="border-bottom p-3" style="min-width: 50px;">Id</th>
                                                <th class="border-bottom p-3" style="min-width: 180px;">Full name</th>
                                                <th class="border-bottom p-3">Mobile</th>
                                                <th class="border-bottom p-3">Email</th>
                                                <th class="border-bottom p-3">Category</th>
                                                <th class="border-bottom p-3">Status</th>
                                                <th class="border-bottom p-3">Staff</th>
                                                <th class="border-bottom p-3" style="min-width: 100px;"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${sessionScope.listCont}" var="c">
                                                <tr>
                                                    <td class="p-3">${c.contact_id}</td>
                                                    <td class="py-3"><span class="ms-2">${c.full_name}</span>
                                                    <td class="p-3">${c.mobile}</td>
                                                    <td class="p-3">${c.email}</td>
                                                    <td class="p-3">${c.setting.setting_title}</td>
                                                    <c:if test="${c.status==0}">
                                                        <td class="p1-3"><span class="badge bg-soft-danger">Processing</span></td>
                                                    </c:if>
                                                    <c:if test="${c.status==1}">
                                                        <td class="p1-3"><span class="badge bg-soft-success">Complete</span></td>
                                                    </c:if>
                                                    <c:if test="${c.staff_id == 0}">
                                                        <td class="p-3">None</td>
                                                    </c:if>
                                                    <c:if test="${c.staff_id != 0}">
                                                        <td class="p-3">${c.staff_id}</td>
                                                    </c:if>
                                                    <td class="text-end p-3">
                                                        <a href="#" class="btn btn-icon btn-pills btn-soft-primary" data-bs-toggle="modal" data-bs-target="#viewprofile${c.contact_id}"><i class="uil uil-eye"></i></a>
                                                        <a href="ContactDetail?contact_id=${c.contact_id}" class="btn btn-icon btn-pills btn-soft-success"><i class="uil uil-pen"></i></a>
                                                        <a href="#" class="btn btn-icon btn-pills btn-soft-danger"><i class="uil uil-trash"></i></a>
                                                    </td>
                                                </tr>
                                                <!-- Profile Start -->
                                            <div class="modal fade" id="viewprofile${c.contact_id}" tabindex="-1" aria-labelledby="exampleModalLabel1" aria-hidden="true">
                                                <div class="modal-dialog modal-lg modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header border-bottom p-3">
                                                            <div class="align-items-center">
                                                                <h5 class="mb-0 ms-3">${c.full_name}</h5>
                                                            </div>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body p-3 pt-4">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="mb-3 d-flex">
                                                                        <h6>Staff:</h6>
                                                                        <c:if test="${c.staff_id == 0}">
                                                                            <p class="text-muted ms-2">None</p>
                                                                        </c:if>
                                                                        <c:if test="${c.staff_id != 0}">
                                                                            <p class="text-muted ms-2">${c.staff_id}</p>
                                                                        </c:if>

                                                                    </div> 
                                                                </div><!--end col-->

                                                                <div class="col-md-6">
                                                                    <div class="mb-3 d-flex">
                                                                        <h6>Category:</h6>
                                                                        <p class="text-muted ms-2">${c.setting.setting_title}</p>
                                                                    </div>                                                                               
                                                                </div><!--end col-->

                                                                <div class="col-md-6">
                                                                    <div class="mb-3 d-flex">
                                                                        <h6>Mobile:</h6>
                                                                        <p class="text-muted ms-2">${c.mobile}</p>
                                                                    </div> 
                                                                </div><!--end col-->

                                                                <div class="col-md-6">
                                                                    <div class="mb-3 d-flex">
                                                                        <h6>Email:</h6>
                                                                        <p class="text-muted ms-2">${c.email}</p>
                                                                    </div>                                                                               
                                                                </div><!--end col-->

                                                                <div class="col-md-12 d-flex">
                                                                    <h6>Message:</h6>
                                                                    <p class="text-muted ms-2">${c.message}</p>
                                                                </div>
                                                            </div><!--end row-->
                                                            <form action="ResponeContact?contact_id=${c.contact_id}&staff_id=${sessionScope.user_id}" method="post"  class="mt-3">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="mb-3">
                                                                            <label class="form-label"><h6>Response:</h6></label>
                                                                            <textarea name="response" rows="4" class="form-control" placeholder="Enter your text" required>${c.response}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div><!--end row-->

                                                                <div class="row">
                                                                    <div class="col-sm-12 text-center">
                                                                        <button type="submit" name="send" class="btn btn-warning">Save</button>
                                                                    </div><!--end col-->
                                                                </div><!--end row-->
                                                            </form><!--end form-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Profile End -->
                                        </c:forEach>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--end row-->

                        <div class="row text-center">
                            <!-- PAGINATION START -->
                            <div class="col-12 mt-4">
                                <div class="d-md-flex align-items-center text-center justify-content-between">
                                    <span class="text-muted me-3">Showing 1 - 10 out of 50</span>
                                    <ul class="pagination justify-content-center mb-0 mt-3 mt-sm-0">
                                        <c:if test="${page!=1}">
                                            <li class="page-item"><a class="page-link" href="ContactList?page=${1}" aria-label="Previous">First</a></li>
                                            <li class="page-item"><a class="page-link" href="ContactList?page=${page-1}" aria-label="Previous">Prev</a></li>
                                            </c:if>
                                            <c:forEach begin="${1}" end="${requestScope.numP}" var="p">
                                            <li class="page-item ${p==page? "active":""}"><a class="page-link" href="ContactList?page=${p}">${p}</a></li>
                                            </c:forEach>
                                            <c:if test="${page!=numP}">
                                            <li class="page-item"><a class="page-link" href="ContactList?page=${page+1}" aria-label="Next">Next</a></li>
                                            <li class="page-item"><a class="page-link" href="ContactList?page=${numP}" aria-label="Previous">Last</a></li>
                                        </c:if>
                                    </ul>
                                </div>
                            </div><!--end col-->
                            <!-- PAGINATION END -->
                        </div><!--end row-->
                    </div>
                </div><!--end container-->

                <!-- Footer Start -->
                <jsp:include page="AdFooter.jsp"></jsp:include>
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->

        <!-- Modal start -->
        <!-- Profile Settings Start -->
        <div class="modal fade" id="editprofile" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header border-bottom p-3">
                        <h5 class="modal-title" id="exampleModalLabel">Contact Details</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-3 pt-4">
                        <div class="row align-items-center">
                            <div class="col-lg-2 col-md-4">
                                <img src="assets/images/doctors/01.jpg" class="avatar avatar-md-md rounded-pill shadow mx-auto d-block" alt="">
                            </div><!--end col-->

                            <div class="col-lg-5 col-md-8 text-center text-md-start mt-4 mt-sm-0">
                                <h6 class="">Upload your picture</h6>
                                <p class="text-muted mb-0">For best results, use an image at least 256px by 256px in either .jpg or .png format</p>
                            </div><!--end col-->

                            <div class="col-lg-5 col-md-12 text-lg-end text-center mt-4 mt-lg-0">
                                <a href="#" class="btn btn-primary">Upload</a>
                                <a href="#" class="btn btn-soft-primary ms-2">Remove</a>
                            </div><!--end col-->
                        </div><!--end row-->

                        <form class="mt-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label">First Name</label>
                                        <input name="name" id="name" type="text" class="form-control" placeholder="First Name :">
                                    </div>
                                </div><!--end col-->

                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label">Last Name</label>
                                        <input name="name" id="name2" type="text" class="form-control" placeholder="Last Name :">
                                    </div>
                                </div><!--end col-->

                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label">Your Email</label>
                                        <input name="email" id="email" type="email" class="form-control" placeholder="Your email :">
                                    </div> 
                                </div><!--end col-->

                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label">Phone no.</label>
                                        <input name="number" id="number" type="text" class="form-control" placeholder="Phone no. :">
                                    </div>                                                                               
                                </div><!--end col-->

                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <label class="form-label">Your Bio Here</label>
                                        <textarea name="comments" id="comments" rows="4" class="form-control" placeholder="Bio :"></textarea>
                                    </div>
                                </div>
                            </div><!--end row-->

                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="submit" id="submit" name="send" class="btn btn-primary" value="Save changes">
                                </div><!--end col-->
                            </div><!--end row-->
                        </form><!--end form-->
                    </div>
                </div>
            </div>
        </div>
        <!-- Profile Settings End -->

        <!-- Modal end -->

        <!-- javascript -->
        <script src="assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="assets/js/simplebar.min.js"></script>
        <!-- Icons -->
        <script src="assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="assets/js/app.js"></script>
        <!-- Table Sort Js -->
        <script src="assets/js/tablesort.js"></script>

    </body>

</html>