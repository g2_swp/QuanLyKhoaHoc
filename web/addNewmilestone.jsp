
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Doctris - Doctor Appointment Booking System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="../../../index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css"/>
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css"/>
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <link href="https://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/csslist.css" rel="stylesheet" type="text/css"/>
        <style>
            .form-label{
                color: #fe7e01;
                font-size: 20px;
                font-family: cursive;
            }
        </style>
    </head>

    <body>
        <!-- Loader -->
        <jsp:include page="Loader.jsp"></jsp:include>
            <!-- Loader -->

            <div class="page-wrapper doctris-theme toggled">

                <!-- sidebar-wrapper  -->
            <jsp:include page="AdSideBar.jsp"></jsp:include>
                <!-- Start Page Content -->
                <main class="page-content bg-light">
                    <div class="top-header">
                        <div class="header-bar d-flex justify-content-between border-bottom">
                        <jsp:include page="AdHeader.jsp"></jsp:include>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="layout-specing">
                            <div class="d-md-flex justify-content-between">
                                <h5 class="mb-0" style="color: orange;font-size: 25px; padding-bottom: 30px;padding-left: 400px">Add New MileStone</h5>  
                            </div>

                            <div class="row mb-5">

                                <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
                                <div class="container bootstrap snippets bootdey">
                                    <div class="col-lg-12">
                                        <div class="main-box clearfix">
                                            <div class="tabs-wrapper tabs-no-header">
                                                <b style="color: palevioletred" id="msg"></b>
                                                <form >
                                                    <div class="tab-content tab-content-body clearfix">
                                                        
                                                                        
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="mb-3">
                                                                                    <label class="form-label">Class Code</label>
                                                                                    <select id="classcode" class="form-control" name="classcode" onchange="handleSelectState(this.value)">
                                                                                    <c:forEach items="${listC}" var="o">
                                                                                        <option value="${o.class_code}">${o.class_code}</option>
                                                                                    </c:forEach>

                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="mb-3">
                                                                                <label class="form-label">Subject Code</label>
                                                                                <select id="subjectcode" class="form-control" name="subjectid" id="subjectcode" onchange="handleSelectState2(this.value)">

                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="mb-3">
                                                                                <label class="form-label">Assignment</label>
                                                                                <select id="ass" class="form-control" name="idAss" id="assTitle"">

                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="mb-3">
                                                                                <label class="form-label">Title</label>
                                                                                <input onblur="validateLenght()" name="title" id="title" type="text" class="form-control">
                                                                                <span id="alert" style="color: red" class=""></span>
                                                                            </div>                                                                               
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="mb-3">
                                                                                <label class="form-label">From Date</label>
                                                                                <input name="fromdate" id="fromDate" type="date" class="form-control">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            <div class="mb-3">
                                                                                <label class="form-label">To Date</label>
                                                                                <input name="todate" id="toDate" type="date" class="form-control" >
                                                                            </div> 
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="mb-3">
                                                                                <label class="form-label">Status</label>
                                                                                <select id="sttus" class="form-control" name="statusM">
                                                                                    <option value="0">DeActive</option>
                                                                                    <option value="1">Active</option>
                                                                                </select>
                                                                            </div>                                                                               
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="mb-3">

                                                                            </div>                                                                               
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="mb-3">
                                                                                <label class="form-label">Description</label>
                                                                                <textarea id="des" class="form-control" name="des" rows="4" cols="50">
                                                                                </textarea>
                                                                            </div>                                                                               
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="mb-3">
                                                                                <label class="form-label">Ass_Body</label>
                                                                                <textarea id="assBody" class="form-control" name="ass_body" rows="4" cols="50">
                                                                                </textarea>
                                                                            </div>                                                                               
                                                                        </div>

                                                    </div>
                                                </div>
                                                <input class="form-label"  style="border: none;float: right;margin-right: 10px"type="submit" onclick="handleAdd()" name="submit" value="Add">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--end container-->
                        <!-- Footer Start -->
                        <jsp:include page="AdFooter.jsp"></jsp:include>
                        <!-- End -->
                        </main>
                        <!--End page-content" -->
                    </div>
                    <!-- javascript -->
                    <script src="assets/js/jquery.min.js" type="text/javascript"></script>
                    <script src="assets/js/bootstrap.bundle.min.js" type="text/javascript"></script>
                    <!-- simplebar -->
                    <script src="assets/js/simplebar.min.js" type="text/javascript"></script>
                    <!-- Select2 -->
                    <script src="assets/js/select2.init.js" type="text/javascript"></script>
                    <script src="assets/js/select2.min.js" type="text/javascript"></script>
                    <!-- Icons -->
                    <script src="assets/js/feather.min.js" type="text/javascript"></script>
                    <!-- Main Js -->
                    <script src="assets/js/app.js" type="text/javascript"></script>
                    <script type="text/javascript">
                                            function validateLenght() {
                                                  let lenght = document.getElementById('title').value.length;
                                                  console.log(lenght);
                                                  if(lenght > 40){
                                                      document.getElementById("alert").innerHTML = "Content is too long";
                                                  }
}
                                                        async function handleAdd() {
                                                            event.preventDefault();
                                                            const classcode = document.getElementById('classcode').value;
                                                            const scode = document.getElementById('subjectcode').value;
                                                            const ass = document.getElementById('ass').value;
                                                            const title = document.getElementById('title').value;
                                                            const stt = document.getElementById('sttus').value;
                                                            const fdate = document.getElementById('fromDate').value;
                                                            const tdate = document.getElementById('toDate').value;
                                                            const des = document.getElementById('des').value;
                                                            const assBody = document.getElementById('assBody').value;
                                                               console.log(stt);
                                                            const res = await fetch('addnewMilestone?id=' + ass + "&cc=" + classcode + "&sid=" + scode
                                                                    + "&tt=" + ass + "&fdate=" + fdate + "&tdate=" + tdate + "&des=" + des + "&abody=" + assBody + "&stt=" + stt, {method: 'post'})
                                                                    .then(res => res.json());

                                                            document.getElementById('msg').innerHTML = res;
                                                        }
                                                        const handleSelectState = (e) => {
                                                            console.log(e);
                                                            getSubject(e);
                                                        };
                                                        const handleSelectState2 = (e) => {
                                                            console.log(e);
                                                            getAssignment(e);
                                                        };
                                                        async function getSubject(classcode) {
                                                            const res = await fetch('subjectListLoad?class_code=' + classcode)
                                                                    .then(res => res.json());
                                                            const data = res.map(c => "<option value=" + c.subject_id + ">" + c.subject_code + "</option>");
                                                            document.getElementById('subjectcode').innerHTML = data;
                                                        }
                                                        async function getAssignment(subjectid) {
                                                            const res = await fetch('assignmentLoad?subjectid=' + subjectid)
                                                                    .then(res => res.json());
                                                            const data = res.map(c => "<option value=" + c.ass_id + ">" + c.subjectName + "</option>");
                                                            document.getElementById('ass').innerHTML = data;
                                                        }
                    </script>
                    </body>

                    </html>