<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Add New Setting</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="../../../index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="course/images/logo.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <style>
            i.fa-regular {
                font-family: fontawesome !important;
            }
            .logo span {
                font-family: 'Open Sans', sans-serif;
                font-size: 30px;
                font-weight: 900;
                vertical-align: middle;
                text-transform: uppercase;
                margin-left: 3px;
            }
        </style>

    </head>

    <body class="bg-dark">

        <div class="page-wrapper doctris-theme">
            <!-- SideBar -->
            <jsp:include page="AdSideBar.jsp"></jsp:include>
                <!-- SideBar  -->

                <!-- Start Page Content -->
                <main class="page-content bg-light">

                    <!-- Header  -->
                <jsp:include page="AdHeader.jsp"></jsp:include>
                    <!-- Header  -->
                    <section class="bg-dashboard">
                        <div class="container">
                            <div class="rounded shadow mt-4">
                                <div class="p-4 border-bottom">
                                    <h5 class="mb-0">Add New Setting:</h5>
                                </div>

                                
                                <div class="p-4">
                                    <form action="SettingController" method="post">
                                        <input type="hidden" name="operation" value="add_new_setting" />
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="form-label">Setting ID</label>
                                                    <br>
                                                    <label class="form-label">${requestScope.setting_id}</label>
                                                <input name="setting_id" id="name" type="hidden" class="form-control"  placeholder="Setting ID:" value="${requestScope.setting_id}" disable>
                                            </div>
                                        </div><!--end col-->

                                        <div class="col-md-6">
                                            
                                                <div class="mb-3">
                                                    <label class="form-label">Type</label>
                                                    <select class="form-select" name="type_id">
                                                        <c:forEach items="${requestScope.type_title_list}" var="t">
                                                            
                                                            <c:if test="${requestScope.setting_details.type_id!=t.setting_id}">
                                                                <option value="${t.setting_id}">${t.setting_title}</option>
                                                            </c:if>

                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            
                                        </div><!--end col-->

                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Setting Title</label>
                                                <input name="setting_title" id="search_form_name" type="text" class="form-control" placeholder="Setting Title:"value="" required="">
                                            </div> 
                                        </div><!--end col-->

                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Setting Value</label>
                                                <input name="setting_value" id="number" type="text" class="form-control" placeholder="Setting Value:"value="">
                                            </div>                                                                               
                                        </div><!--end col-->

                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Display Order</label>
                                                <input name="display_order" id="number" type="number" min="0" class="form-control" placeholder="Display Order:"value="0">
                                            </div>                                                                               
                                        </div><!--end col-->

                                        <div class="col-md-6">
                                            <div class="mb-3">

                                                <label class="form-label">Status</label>
                                                
                                                    <div class="form-check">
                                                        <input type="radio" name="status" value="1" id="Active" class="form-check-input" checked="checked"/>
                                                        <label for="Active" class="form-check-label">Active</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input type="radio" name="status" value="0" id="Inactive" class="form-check-input"/>
                                                        <label for="Inactive" class="form-check-label">Inactive</label>
                                                    </div>
                                                
                                                


                                            </div>                                                                               
                                        </div><!--end col-->

                                        <div class="col-md-12">
                                            <div class="mb-3">
                                                <label class="form-label">Description</label>
                                                <textarea name="description" id="comments" rows="4" class="form-control" placeholder="description :"value=""></textarea>
                                            </div>
                                        </div>
                                    </div><!--end row-->
                                    
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="submit" id="submit" name="send" class="btn btn-primary" value="Save changes" onclick="return confirm('Are you sure?')" >
                                            </div><!--end col-->
                                        </div><!--end row-->
                                    
                                </form><!--end form--> 
                            </div>
                        </div>
                    </div><!--end container-->
                </section><!--end section-->

                <!-- Footer Start -->
                <jsp:include page="AdFooter.jsp"></jsp:include>
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->

        <!-- javascript -->
        <script src="assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="assets/js/simplebar.min.js"></script>
        <!-- Icons -->
        <script src="assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="assets/js/app.js"></script>

    </body>

</html>