<%-- 
    Document   : SideBar
    Created on : Sep 20, 2022, 12:53:38 AM
    Author     : MEP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- SideBar -->
<nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">
        <div class="sidebar-brand">
            <a href="index.html">
                <h3><img src="assets/images/logo_2.png" height="35" class="logo-light-mode" alt="">
                    <span class="text-dark">Course</span></h3>
            </a>
        </div>

        <ul class="sidebar-menu pt-3">
            <li><a href="dashboad.jsp"><i class="uil uil-dashboard me-2 d-inline-block"></i>Dashboard</a></li>

            <li class="sidebar-dropdown">
                <a href="javascript:void(0)"><i class="uil uil-user me-2 d-inline-block"></i>User Manager</a>
                <div class="sidebar-submenu">
                    <ul>
                        <li><a href="doctors.html">User List</a></li>
                        <li><a href="add-doctor.html">User Details</a></li>
                    </ul>
                </div>
            </li>

            <li class="sidebar-dropdown">
                <a href="javascript:void(0)"><i class="uil uil-apps me-2 d-inline-block"></i>Apps</a>
                <div class="sidebar-submenu">
                    <ul>
                        <li><a href="chat.html">Chat</a></li>
                        <li><a href="email.html">Email</a></li>
                        <li><a href="calendar.html">Calendar</a></li>
                    </ul>
                </div>
            </li>

            <li class="sidebar-dropdown">
                <a href="javascript:void(0)"><i class="uil uil-shopping-cart me-2 d-inline-block"></i>Pharmacy</a>
                <div class="sidebar-submenu">
                    <ul>
                        <li><a href="shop.html">Shop</a></li>
                        <li><a href="product-detail.html">Shop Detail</a></li>
                        <li><a href="shopcart.html">Shopcart</a></li>
                        <li><a href="checkout.html">Checkout</a></li>
                    </ul>
                </div>
            </li>

            <li class="sidebar-dropdown">
                <a href="javascript:void(0)"><i class="uil uil-setting me-2 d-inline-block"></i>Setting</a>
                <div class="sidebar-submenu">
                    <ul>
                        <li><a href="faqs.html">Setting List</a></li>
                        <li><a href="review.html">Setting Details</a></li>
                    </ul>
                </div>
            </li>

            <li><a href="components.html"><i class="uil uil-globe d-inline-block"></i>Contact</a></li>

            <li class="sidebar-dropdown">
                <a href="javascript:void(0)"><i class="uil uil-book-open me-2 d-inline-block"></i>Subject</a>
                <div class="sidebar-submenu">
                    <ul>
                        <li><a href="SubjectController">Subject List</a></li>
                    </ul>
                </div>
            </li>
            
        </ul>
        <!-- sidebar-menu  -->
    </div>

</nav>
<!-- SideBar  -->
