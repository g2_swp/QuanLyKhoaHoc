<%-- 
    Document   : Loader
    Created on : Sep 20, 2022, 12:49:10 AM
    Author     : MEP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>
</div>
<!-- Loader -->
