<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Eval-criteria Detail</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="../../../index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="course/images/logo.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <style>
            i.fa-regular {
                font-family: fontawesome !important;
            }
            .logo span {
                font-family: 'Open Sans', sans-serif;
                font-size: 30px;
                font-weight: 900;
                vertical-align: middle;
                text-transform: uppercase;
                margin-left: 3px;
            }
        </style>

    </head>

    <body class="bg-dark">

        <div class="page-wrapper doctris-theme">
            <!-- SideBar -->
            <jsp:include page="AdSideBar.jsp"></jsp:include>
                <!-- SideBar  -->

                <!-- Start Page Content -->
                <main class="page-content bg-light">

                    <!-- Header  -->
                <jsp:include page="AdHeader.jsp"></jsp:include>
                    <!-- Header  -->
                    <section class="bg-dashboard">
                        <div class="container">
                            <div class="rounded shadow mt-4">
                                <div class="bg-warning">
                                    <div class="p-4 border-bottom">
                                        <h5 class="mb-0">Eval-criteria Detail</h5>
                                </div>
                            </div>

                            <div class="p-4">
                                <form action="EvalDetail?criteria_id=${e.criteria_id}" method="post" class="mt-3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="mb-3 d-flex">
                                                <h6>Assignment:</h6>
                                                <p class="text-muted ms-2">${e.assignment.title}</p>
                                            </div>                                                                               
                                        </div>

                                        <div class="col-md-6">
                                            <div class="mb-3 d-flex">
                                                <h6>Milestone:</h6>
                                                <p class="text-muted ms-2">${e.milestone_id}</p>
                                            </div> 
                                        </div><!--end col-->

                                        <div class="col-md-12">
                                            <div class="mb-3 ">
                                                <label class="form-label"><h6>Criteria Name:</h6></label>
                                                <input name="name" type="text" class="form-control" value="${e.criteria_name}">
                                            </div>                                                                               
                                        </div><!--end col-->

                                        <div class="col-md-6">
                                            <div class="mb-3 ">
                                                <label class="form-label"><h6>Team Eval:</h6></label>
                                                <input name="team" type="text" class="form-control" value="${e.is_team_eval}">
                                            </div> 
                                        </div>

                                        <div class="col-md-6">
                                            <div class="mb-3 ">
                                                <label class="form-label"><h6>Eval Weight:</h6></label>
                                                <input name="weight" type="text" class="form-control" value="${e.eval_weight}">
                                            </div>                                                                               
                                        </div><!--end col-->
                                        
                                        <div class="col-md-6">
                                            <div class="mb-3 ">
                                                <label class="form-label"><h6>Max LOC:</h6></label>
                                                <input name="loc" type="text" class="form-control" value="${e.max_loc}">
                                            </div> 
                                        </div>

                                        <div class="col-md-6">
                                            <div class="mb-3 ">
                                                <h6>Status:</h6>
                                                <select class="form-select" name="status">
                                                    <option value="0">Processing</option>
                                                    <option value="1" ${e.status == 1 ?"selected":""}>Complete</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div><!--end row-->
                                    <div class="col-md-12">
                                        <div class="mb-3">
                                            <label class="form-label"><h6>Description:</h6></label>
                                            <textarea name="description" rows="4" class="form-control" placeholder="Enter your text" required>${e.description}</textarea>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12 text-center">
                                            <button type="submit" name="send" class="btn btn-warning">Save</button>
                                        </div><!--end col-->
                                    </div><!--end row-->
                                </form><!--end form-->
                            </div>
                        </div>
                    </div><!--end container-->
                </section><!--end section-->

                <!-- Footer Start -->
                <jsp:include page="AdFooter.jsp"></jsp:include>
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->

        <!-- javascript -->
        <script src="assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="assets/js/simplebar.min.js"></script>
        <!-- Icons -->
        <script src="assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="assets/js/app.js"></script>

    </body>

</html>