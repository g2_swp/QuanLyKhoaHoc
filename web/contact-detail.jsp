<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Contact Detail</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="../../../index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="course/images/logo.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <style>
            i.fa-regular {
                font-family: fontawesome !important;
            }
            .logo span {
                font-family: 'Open Sans', sans-serif;
                font-size: 30px;
                font-weight: 900;
                vertical-align: middle;
                text-transform: uppercase;
                margin-left: 3px;
            }
        </style>

    </head>

    <body class="bg-dark">

        <div class="page-wrapper doctris-theme">
            <!-- SideBar -->
            <jsp:include page="AdSideBar.jsp"></jsp:include>
                <!-- SideBar  -->

                <!-- Start Page Content -->
                <main class="page-content bg-light">

                    <!-- Header  -->
                <jsp:include page="AdHeader.jsp"></jsp:include>
                    <!-- Header  -->
                    <section class="bg-dashboard">
                        <div class="container">
                            <div class="rounded shadow mt-4">
                                <div class="bg-warning">
                                <div class="p-4 border-bottom">
                                    <h5 class="mb-0">${c.full_name}</h5>
                            </div>
                            </div>

                            <div class="p-4">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-3 d-flex">
                                            <h6>Staff:</h6>
                                            <c:if test="${c.staff_id == 0}">
                                                <p class="text-muted ms-2">None</p>
                                            </c:if>
                                            <c:if test="${c.staff_id != 0}">
                                                <p class="text-muted ms-2">${c.staff_id}</p>
                                            </c:if>

                                        </div> 
                                    </div><!--end col-->

                                    <div class="col-md-6">
                                        <div class="mb-3 d-flex">
                                            <h6>Category:</h6>
                                            <p class="text-muted ms-2">${c.setting.setting_title}</p>
                                        </div>                                                                               
                                    </div><!--end col-->

                                    <div class="col-md-6">
                                        <div class="mb-3 d-flex">
                                            <h6>Mobile:</h6>
                                            <p class="text-muted ms-2">${c.mobile}</p>
                                        </div> 
                                    </div><!--end col-->

                                    <div class="col-md-6">
                                        <div class="mb-3 d-flex">
                                            <h6>Email:</h6>
                                            <p class="text-muted ms-2">${c.email}</p>
                                        </div>                                                                               
                                    </div><!--end col-->

                                    <div class="col-md-12 d-flex">
                                        <h6>Message:</h6>
                                        <p class="text-muted ms-2">${c.message}</p>
                                    </div>
                                </div><!--end row-->
                                <form action="ResponeContact?contact_id=${c.contact_id}&staff_id=${sessionScope.user_id}" method="post"  class="mt-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="mb-3">
                                                <label class="form-label"><h6>Response:</h6></label>
                                                <textarea name="response" rows="4" class="form-control" placeholder="Enter your text" required>${c.response}</textarea>
                                            </div>
                                        </div>
                                    </div><!--end row-->

                                    <div class="row">
                                        <div class="col-sm-12 text-center">
                                            <button type="submit" name="send" class="btn btn-warning">Save</button>
                                        </div><!--end col-->
                                    </div><!--end row-->
                                </form><!--end form-->
                            </div>
                        </div>
                    </div><!--end container-->
                </section><!--end section-->

                <!-- Footer Start -->
                <jsp:include page="AdFooter.jsp"></jsp:include>
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->

        <!-- javascript -->
        <script src="assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="assets/js/simplebar.min.js"></script>
        <!-- Icons -->
        <script src="assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="assets/js/app.js"></script>

    </body>

</html>