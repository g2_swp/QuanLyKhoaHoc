<%-- 
    Document   : newjsp
    Created on : Oct 4, 2022, 1:21:40 AM
    Author     : hungl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="https://shreethemes.in" />
        <meta name="Version" content="v1.2.0" />

        <!-- favicon -->
        <link rel="shortcut icon" href="course/images/logo.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <!-- ReCaptcha -->
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>

        <style>
            .g-recaptcha {
                margin: 15px auto !important;
                width: auto !important;
                height: auto !important;
                text-align: -webkit-center;
                text-align: -moz-center;
                text-align: -o-center;
                text-align: -ms-center;
            }
            i.fa-regular {
                font-family: fontawesome !important;
            }
            .logo span {
                font-family: 'Open Sans', sans-serif;
                font-size: 30px;
                font-weight: 900;
                vertical-align: middle;
                text-transform: uppercase;
                margin-left: 3px;
            }
        </style>

    </head>

    <body>

        <div class="back-to-home rounded d-none d-sm-block">
            <a href="index.html" class="btn btn-icon btn-warning"><i data-feather="home" class="icons"></i></a>
        </div>

        <!-- Hero Start -->
        <section class="bg-home d-flex bg-light align-items-center hero_slide_background" style="background-image:url(course/images/slider_background.jpg)">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-md-8">
                        <a href="#" class="logo-icon logo text-center pt-3">
                            <h3><img src="course/images/logo.png" height="35" class="logo-light-mode" alt="">
                                <span class="text-white">Course</span></h3>
                        </a>
                        <div class="card login-page bg-white shadow mt-4 rounded border-0">
                            <div class="card-body">
                                <h4 class="text-center">Sign In</h4>  
                                <form action="Login" class="login-form mt-4" method="post">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="mb-3">
                                                <label class="form-label">Your Email <span class="text-danger">*</span></label>
                                                <input type="email" class="form-control" placeholder="Email" name="email" required="">
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="mb-3">
                                                <label class="form-label">Password <span class="text-danger">*</span></label>
                                                <input type="password" class="form-control" placeholder="Password" name ="password" required="">
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="mb-3"style="color:red">
                                                <jsp:include page="notification.jsp"></jsp:include>
                                            </div>
                                        </div>


                                        <div class="col-lg-12">
                                            <div class="d-flex justify-content-between">
                                                <div class="mb-3">

                                                </div>
                                                <a href="forgotPassword" class="text-dark h6 mb-0">Forgot password ?</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 text-xs-center">
                                            <div class="g-recaptcha"
                                                 data-sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"></div>
                                        </div>
                                        <div class="col-lg-12 mb-0">
                                            <div class="d-grid">
                                                <button class="btn btn-warning">Login</button>
                                            </div>
                                        </div>
                                </form>
                                <div class="col-lg-12 mt-3 text-center">
                                    <h6 class="text-muted">Or</h6>
                                </div><!--end col-->



                                <!--                                        <div class="col-12 mt-3">
                                                                            <div class="d-grid g-signin2" data-onsuccess="onSignIn">
                                                                                <a href="#" class="btn btn-soft-danger" data-onsuccess="onSignIn"><i class="uil uil-google"></i> Login with Google</a>
                                                                            </div>
                                                                        </div>end col-->

                                <div class="col-12 mt-3">

                                    <div class="d-grid">
                                        <a class="btn btn-soft-danger" href="https://accounts.google.com/o/oauth2/auth?scope=email&redirect_uri=http://localhost:8080/TrainingSupportSystem/LoginWithGoogle&response_type=code
                                           &client_id=106270957735-vq6n56tdsivq25k1jal5a4rhrtm9n83c.apps.googleusercontent.com&approval_prompt=force">Login With Google</a>  
                                    </div>
                                </div><!--end col-->

                                <div class="col-12 text-center mt-4">
                                    <p class="mb-0 mt-3 d-inline"><small class="text-dark me-2">Don't have an account ?</small> 
                                    <form action="UserController" class="d-inline" method="POST">
                                        <input type="hidden" name="operation" value="view_register_page" />
                                        <input type="submit" id="submit" name="send" class="text-dark fw-bold" value="Sign Up" style="border:none;background: none">
                                    </form>
<!--                                        
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div><!---->
                </div> <!--end col-->
            </div><!--end row-->
        </div> <!--end container-->
    </section><!--end section-->
    <!-- Hero End -->

    <!-- javascript -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <!-- Icons -->
    <script src="assets/js/feather.min.js"></script>
    <!-- Main Js -->
    <script src="assets/js/app.js"></script>

</body>
</html>
