<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Edit Subject</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="../../../index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="course/images/logo.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <style>
            i.fa-regular {
                font-family: fontawesome !important;
            }
            .logo span {
                font-family: 'Open Sans', sans-serif;
                font-size: 30px;
                font-weight: 900;
                vertical-align: middle;
                text-transform: uppercase;
                margin-left: 3px;
            }
        </style>

    </head>

    <body class="bg-dark">
        <!-- Loader -->
        <jsp:include page="Loader.jsp"></jsp:include>
            <!-- Loader -->

            <div class="page-wrapper doctris-theme">
                <!-- SideBar -->
            <jsp:include page="AdSideBar.jsp"></jsp:include>
                <!-- SideBar  -->

                <!-- Start Page Content -->
                <main class="page-content bg-light">

                    <!-- Header  -->
                <jsp:include page="AdHeader.jsp"></jsp:include>
                    <!-- Header  -->
                    <section class="bg-dashboard">
                        
                        <div class="container">
                            <p style="color: blue" id="nota">${nota}</p>
                            <div class="rounded shadow mt-4">
                                <div class="p-4 border-bottom">
                                    <h5 class="mb-0">Edit Subject Details</h5>
                                </div>

                                <div class="p-4">
                                    <form id="myForm" action="editController" method="post">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="form-label">ID</label>
                                                    <input name="subjectid" type="text" class="form-control" style="background-color: #d4d4d2" value="${subject.subject_id}" readonly>
                                            </div>
                                        </div><!--end col-->

                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Code</label>
                                                <input name="code" id="name2" type="text" class="form-control"  value="${subject.subject_code}" >
                                            </div>
                                        </div><!--end col-->

                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Name</label>
                                                <input name="name" id="name" type="text" class="form-control"value="${subject.subject_name}">
                                            </div> 
                                        </div><!--end col-->

                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Manager</label>
                                                <select name="manager" class="form-control">
                                                    <c:forEach items="${userListM}" var="o">
                                                        <option <c:if test="${o.fullname eq subject.manager}">selected</c:if> value="${o.user_id}">${o.fullname}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>                                                                               
                                        </div>

                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Expert</label>
                                               <select name="expert" class="form-control">
                                                    <c:forEach items="${userListE}" var="o">
                                                        <option <c:if test="${o.fullname.equals(subject.expert) }">selected</c:if> value="${o.user_id}">${o.fullname}</option>
                                                    </c:forEach>
                                                </select>
                                                
                                            </div>                                                                               
                                        </div>

                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Status</label>
                                                <select name="status" class="form-control">
                                                    <option <c:if test="${subject.status==1}">selected</c:if> value="1">Active</option>
                                                    <option <c:if test="${subject.status==0}">selected</c:if> value="0">DeActive</option>
                                                </select>
                                            </div>                                                                               
                                        </div>

                                        <div class="col-md-12">
                                            <div class="mb-3">
                                                <label class="form-label">Description</label>
                                                <textarea name="des" id="comments" rows="4" class="form-control">${subject.body}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="submit" id="submit" name="send" onclick="saveChange()" class="btn btn-primary" value="Save changes">
                                        </div><!--end col-->
                                    </div><!--end row-->
                                </form><!--end form--> 
                            </div>
                        </div>
                    </div><!--end container-->
                </section><!--end section-->
                <a style="margin-left: 1300px"  class="btn btn-primary" href="SubjectController">Back List Subject</a>
                <!-- Footer Start -->
                <jsp:include page="AdFooter.jsp"></jsp:include>
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->

        <!-- javascript -->
        <script src="assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="assets/js/simplebar.min.js"></script>
        <!-- Icons -->
        <script src="assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="assets/js/app.js"></script>
        <script type="text/javascript">
            function saveChange(){
                let text = "Are you sure Save Change?";
                if(confirm(text) == true){
                    document.getElementById("myForm").submit();
                }else{
                    alert("Edit fail!");
                }
            }
        </script>
    </body>

</html>