<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/moment@2.24.0/moment.min.js"></script>
        <title>AdminSite</title>
    </head>
    <body>

        <!-- SIDEBAR -->
        <section id="sidebar">
          <jsp:include page="AdSideBar.jsp"></jsp:include>
        </section>
        <!-- SIDEBAR -->

        <!-- NAVBAR -->
        <section id="content">
          
            <!-- NAVBAR -->

            <!-- MAIN -->
            <main>
                <h1 class="title">Dashboard</h1>
                <ul class="breadcrumbs">
                    <li><a href="#">Home</a></li>
                    <li class="divider">/</li>
                    <li><a href="#" class="active">Dashboard</a></li>
                </ul>
                <div>
                    Date: <input type="text" id="datepicker">
                    States: <select onchange="handleSelectState(this.value)" style="width: 100px" id="states">
                    </select>
                    Districts:<select style="width: 100px" id="dist">
                    </select>
                    <input type="submit"  onclick="onSubmit()" value="Search"/>
                </div>
                <div class="info-data">
                    <div class="card">
                        <div class="head">
                            <div>
                                <h2 id="confirmed"></h2>
                                <p>Confirmed</p>
                            </div>
                            <i class='bx bx-trending-up icon' ></i>
                        </div>
                        <span class="progress" data-value="40%"></span>
                        <span class="label" id="cf"></span>
                    </div>
                    <div class="card">
                        <div class="head">
                            <div>
                                <h2 id="deceased"></h2>
                                <p>Deceased</p>
                            </div>
                            <i class='bx bx-trending-down icon down' ></i>
                        </div>
                        <span class="progress" data-value="60%"></span>
                        <span class="label" id="de"></span>
                    </div>
                    <div class="card">
                        <div class="head">
                            <div>
                                <h2 id="recovered"></h2>
                                <p>Recovered</p>
                            </div>
                            <i class='bx bx-trending-up icon' ></i>
                        </div>
                        <span class="progress" data-value="30%"></span>
                        <span class="label" id="re"></span>
                    </div>
                    <div class="card">
                        <div class="head">
                            <div>
                                <h2 id="tested"></h2>
                                <p>Tested</p>
                            </div>
                            <i class='bx bx-trending-up icon' ></i>
                        </div>
                        <span class="progress" data-value="80%"></span>
                        <span class="label" id="te"></span>
                    </div>
                </div>
                <div class="data">
                    <div class="content-data">
                        <div class="head">
                            <h3>Sales Report</h3>
                            <div class="menu">
                                <i class='bx bx-dots-horizontal-rounded icon'></i>
                                <ul class="menu-link">
                                    <li><a href="#">Edit</a></li>
                                    <li><a href="#">Save</a></li>
                                    <li><a href="#">Remove</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="chart">
                            <div id="chart"></div>
                        </div>
                    </div>
                    <div class="content-data">
                        <div class="head">
                            <h3>Chatbox</h3>
                            <div class="menu">
                                <i class='bx bx-dots-horizontal-rounded icon'></i>
                                <ul class="menu-link">
                                    <li><a href="#">Edit</a></li>
                                    <li><a href="#">Save</a></li>
                                    <li><a href="#">Remove</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="chat-box">
                            <p class="day"><span>Today</span></p>
                            <div class="msg">
                                <img src="https://images.unsplash.com/photo-1517841905240-472988babdf9?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8cGVvcGxlfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" alt="">
                                <div class="chat">
                                    <div class="profile">
                                        <span class="username">Alan</span>
                                        <span class="time">18:30</span>
                                    </div>
                                    <p>Hello</p>
                                </div>
                            </div>
                            <div class="msg me">
                                <div class="chat">
                                    <div class="profile">
                                        <span class="time">18:30</span>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque voluptatum eos quam dolores eligendi exercitationem animi nobis reprehenderit laborum! Nulla.</p>
                                </div>
                            </div>
                            <div class="msg me">
                                <div class="chat">
                                    <div class="profile">
                                        <span class="time">18:30</span>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam, architecto!</p>
                                </div>
                            </div>
                            <div class="msg me">
                                <div class="chat">
                                    <div class="profile">
                                        <span class="time">18:30</span>
                                    </div>
                                    <p>Lorem ipsum, dolor sit amet.</p>
                                </div>
                            </div>
                        </div>
                        <form action="#">
                            <div class="form-group">
                                <input type="text" placeholder="Type...">
                                <button type="submit" class="btn-send"><i class='bx bxs-send' ></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </main>
            <!-- MAIN -->
        </section>
        <!-- NAVBAR -->
 <jsp:include page="AdFooter.jsp"></jsp:include>
        <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
        <script src="script.js"></script>
        <script>

                        const handleSelectState = (e) => {
                            console.log(e);
                            getDistricts(e);
                        };
                        async  function  onSubmit() {
                            var date = document.getElementById("datepicker").value;
                            var dateformat = dateFormat(date, 'yyyy-MM-dd');
                            var stateCode = document.getElementById("states").value;
                            var dis = document.getElementById("dist").value;
                            var listdate = getDates(dateformat);
                            var confirmed = [];
                            var deceased = [];
                            var recovered = [];
                            var tested = [];

                            console.log(listdate);
                            getData(dateformat, stateCode, dis);
                            const res = fetch("https://data.covid19india.org/v4/min/timeseries-"+stateCode+".min.json").then(res => res.json())
                            const resjson = await res;
                            console.log(resjson);
                            const data = resjson[stateCode]["districts"][dis]["dates"];

                            const list = [];
                            for (var item of listdate) {
                                confirmed.push(data[item]['total'].confirmed);
                                deceased.push(data[item]['total'].deceased);
                                recovered.push(data[item]['total'].recovered);
                                tested.push(data[item]['total'].tested);
                            }
                            var options = {
                                series: [{
                                        name: 'confirmed',
                                        data: confirmed
                                    }, {
                                        name: 'deceased',
                                        data: deceased
                                    },
                                    {
                                        name: 'recovered',
                                        data: recovered
                                    }, {
                                        name: 'tested',
                                        data: tested
                                    }],
                                chart: {
                                    height: 350,
                                    type: 'area'
                                },
                                dataLabels: {
                                    enabled: false
                                },
                                stroke: {
                                    curve: 'smooth'
                                },
                                xaxis: {
                                    type: 'date',
                                    categories: getDates(dateformat)
                                },
                                tooltip: {
                                    x: {
                                        format: 'dd/MM/yy'
                                    },
                                },
                            };
                                document.querySelector("#chart").innerHTML="";
                            var chart = new ApexCharts(document.querySelector("#chart"), options);
                            chart.render();
                        }
                        ;

                        async function getData(date, stateCode, dist) {
                            const res = await fetch('https://data.covid19india.org/v4/min/data-' + date + '.min.json')
                                    .then(res => res.json());
//                            console.log(res);
                            const data = res[stateCode].districts[dist];
//                            console.log(data);
                            var totalcase = data.total.confirmed + data.total.deceased
                                    + data.total.recovered + data.total.tested;
                            document.getElementById("confirmed").innerHTML = data.total.confirmed;
                            document.getElementById("cf").innerHTML = data.total.confirmed / totalcase * 100;
                            document.getElementById("deceased").innerHTML = data.total.deceased;
                            document.getElementById("de").innerHTML = data.total.deceased / totalcase * 100;
                            document.getElementById("recovered").innerHTML = data.total.recovered;
                            document.getElementById("re").innerHTML = data.total.recovered / totalcase * 100;
                            document.getElementById("tested").innerHTML = data.total.tested;
                            document.getElementById("te").innerHTML = data.total.tested / totalcase * 100;
//                            console.log(data.total);
//                            console.log(totalcase);
                            return  res;
                        }

                        async function getStates() {
                            const res = await fetch('https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/states.json')
                                    .then(res => res.json());
                            const data = res.filter(c => c.country_id === 101).map(c => "<option value='" + c.state_code + "'>" + c.state_code + "</option>");
                            console.log(data);
                            document.getElementById('states').innerHTML = data;
                        }
                        async function getDistricts(stateCode) {
                            const res = await fetch('https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/cities.json')
                                    .then(res => res.json());
                            const data = res.filter(c => c.country_id == 101 && c.state_code == stateCode).map(c => "<option>" + c.name + "</option>");
                            district = data;
                            document.getElementById('dist').innerHTML = data;
                        }
                        $("#datepicker").datepicker();
                        getStates();
                        //a simple date formatting function
                        function dateFormat(inputDate, format) {
                            //parse the input date
                            const date = new Date(inputDate);

                            //extract the parts of the date
                            const day = date.getDate();
                            const month = date.getMonth() + 1;
                            const year = date.getFullYear();

                            //replace the month
                            format = format.replace("MM", month.toString().padStart(2, "0"));

                            //replace the year
                            if (format.indexOf("yyyy") > -1) {
                                format = format.replace("yyyy", year.toString());
                            } else if (format.indexOf("yy") > -1) {
                                format = format.replace("yy", year.toString().substr(2, 2));
                            }

                            //replace the day
                            format = format.replace("dd", day.toString().padStart(2, "0"));

                            return format;
                        }
                        function getDates(date) {
                            var dateArray = [];
                            var i = 9;
                            while (i > -1) {
                                var date2 = moment(date).subtract(i, 'day');
                                i--;
                                dateArray.push(dateFormat(date2, "yyyy-MM-dd"));
                            }
                            return dateArray;
                        }

        </script>
    </body>
</html>