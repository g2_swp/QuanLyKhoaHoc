
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>List Subject</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="../../../index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="../assets/images/favicon.ico.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css"/>
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css"/>
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css"/>
        <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />

    </head>
    <style>
        .active, .btn:hover {
            color: #25efdf;
        }
        table td{
            margin-right: 20px
        }
        #text{
            display: none;
        }
       
    </style>
    <body>
        <!-- Loader -->
        <jsp:include page="Loader.jsp"></jsp:include>
            <!-- Loader -->

            <div class="page-wrapper doctris-theme toggled">
            <jsp:include page="AdSideBar.jsp"></jsp:include>
                <!-- sidebar-wrapper  -->

                <!-- Start Page Content -->
                <main class="page-content bg-light">

                    <div class="top-header">
                        <div class="header-bar d-flex justify-content-between border-bottom">

                        <jsp:include page="AdHeader.jsp"></jsp:include>

                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="layout-specing">
                            <div class="d-md-flex justify-content-between">
                                <h5 style="margin: 0 auto;color: orange;font: 30px" class="mb-0">Subject List  Of Training Support System</h5>                
                            </div>

                            <form style="margin-top: 30px"  method="post" action="SearchController">
                                <table>
                                    <tr>
                                        <td> 
                                            <label class="form-label"> Search by </label>
                                            <select name="option" style="border: none" id="option">
                                                <option <c:if test="${option.equals('code')}">selected</c:if> value="code">Code</option>
                                            <option <c:if test="${option.equals('name')}">selected</c:if> value="name">Name</option>
                                            </select></td>
                                        <td> <label class="form-label"> Status </label></td>
                                        <td> <label class="form-label"> Manager </label></td>
                                        <td> <label class="form-label"> Expert </label></td>
                                    </tr>
                                    <tr>
                                        <td>   <input id="keyword" type="text" placeholder="Search.." value="${keyword}" name="keyword"></td>
                                    <td> 
                                        <select id="st" name="status" style="width: 150px;height: 29px">
                                            <option value="1">Active</option>
                                            <option value="0">DeActive</option>
                                            <option value="2">All</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="manager" name="manager" style="width: 150px;height: 29px">
                                            <c:forEach var="o" items="${userListM}">
                                                <option value="${o.fullname}">${o.fullname}</option>
                                            </c:forEach>
                                            <option value="2">All</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="expert" name="expert" style="width: 150px;height: 29px">
                                            <c:forEach var="o" items="${userListE}">
                                                <option  value="${o.fullname}">${o.fullname}</option>
                                            </c:forEach>
                                            <option value="2">All</option>
                                        </select>
                                    </td>
                                    <td> <button type="submit">Search</button></td>
                                </tr>
                            </table>
                        </form>

                        <div class="row">
                            <div class="col-12 mt-4">
                                <div class="table-responsive shadow rounded">
                                    <table class="table table-center bg-white mb-0">
                                        <thead>
                                            <tr>
                                                <th class="border-bottom p-3" style="min-width: 80px;">Id
                                                    <a onclick="setSort('Id', 'asc')"> <i class='fas fa-caret-up '></i></a> 
                                                    <a onclick="setSort('Id', 'desc')"><i class='fas fa-sort-down'></i></a></th>
                                                <th class="border-bottom p-3" style="min-width: 180px;">Code
                                                    <a onclick="setSort('code', 'asc')"> <i class='fas fa-caret-up'></i></a> 
                                                    <a onclick="setSort('code', 'desc')"><i class='fas fa-sort-down'></i></a></th>
                                                <th class="border-bottom p-3">Name
                                                    <a onclick="setSort('name', 'asc')"> <i class='fas fa-caret-up'></i></a> 
                                                    <a onclick="setSort('name', 'desc')"><i class='fas fa-sort-down'></i></a></th>
                                                <th class="border-bottom p-3">Manager
                                                    <a onclick="setSort('manager', 'asc')"> <i class='fas fa-caret-up'></i></a> 
                                                    <a onclick="setSort('manager', 'desc')"><i class='fas fa-sort-down'></i></a></th>
                                                <th class="border-bottom p-3">Expert
                                                    <a onclick="setSort('expert', 'asc')"> <i class='fas fa-caret-up'></i></a> 
                                                    <a onclick="setSort('expert', 'desc')"><i class='fas fa-sort-down'></i></a></th>
                                                <th class="border-bottom p-3">Status
                                                    <a onclick="setSort('status', 'asc')"> <i class='fas fa-caret-up'></i></a> 
                                                    <a onclick="setSort('expert', 'desc')"><i class='fas fa-sort-down'></i></a></th>
                                                <th class="border-bottom p-3"></th>
                                            </tr>
                                        </thead>
                                        <tbody id="bodytable">
                                            <c:forEach var="c" items="${subjectList}">
                                                <tr>
                                                    <th class="p-3">${c.subject_id}</th>
                                                    <td class="py-3">
                                                        <a href="#" class="text-dark">
                                                            <div class="d-flex align-items-center">
                                                                <span class="ms-2">${c.subject_code}</span>
                                                            </div>
                                                        </a>
                                                    </td>
                                                    <td class="p-3">${c.subject_name}</td>
                                                    <td class="p-3">${c.manager}</td>
                                                    <td class="p-3">${c.expert}</td>
                                                    <td class="p-3" style="color: blue">
                                                        <c:if test="${c.status==1}">Active</c:if>
                                                        <c:if test="${c.status==0}">DeActive</c:if>
                                                        </td>
                                                        <td class="text-end p-3">
                                                            <a  onclick="viewDetail(${c.subject_id})" class="btn btn-icon btn-pills btn-soft-primary" data-bs-toggle="modal" data-bs-target="#viewprofile">
                                                            <i class="uil uil-eye"></i>
                                                        </a>
                                                        <a href="editController?id=${c.subject_id}">
                                                            <i class="uil uil-pen"></i></a>
                                                        <a href="addController">
                                                            <i class="fa-sharp fa-solid fa-plus"></i></a>
                                                    </td>

                                                </tr>
                                            </c:forEach>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--end row-->

                        <div class="row text-center">
                            <!-- PAGINATION START -->
                            <div class="col-12 mt-4">
                                <div class="d-md-flex align-items-center text-center justify-content-between">
                                    <ul class="pagination justify-content-center mb-0 mt-3 mt-sm-0" id="pagetotal">
                                        <li class="page-item"><a class="page-link" href="javascript:void(0)" aria-label="Previous">Prev</a></li>
                                            <c:forEach begin="1" end="${sessionScope.endP}" var="i" >
                                            <li class="page-item <c:if test="${i eq 1}">active</c:if> " onclick="nextPage(${i})"><a class="page-link" >${i}</a></li>
                                            </c:forEach>
                                        <li class="page-item"><a class="page-link" href="javascript:void(0)" aria-label="Next">Next</a></li>
                                    </ul>
                                </div>
                            </div><!--end col-->
                            <!-- PAGINATION END -->
                        </div><!--end row-->
                    </div>
                </div><!--end container-->

                <!-- Footer Start -->

                <!-- End -->
            </main>
            <!-- Profile Start -->
            <div class="modal fade" id="viewprofile" tabindex="-1" aria-labelledby="exampleModalLabel1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header border-bottom p-3">
                            <h5 class="modal-title" id="exampleModalLabel1">Subject Detail</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body p-3 pt-4">

                            <ul class="list-unstyled mb-0 d-md-flex justify-content-between mt-4">
                                <li>
                                    <ul id="subject" class="list-unstyled mb-0">
                                    </ul>

                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Profile End -->
            <!-- Modal end -->
            <jsp:include page="AdFooter.jsp"></jsp:include>
            <!-- javascript -->
            <script src="assets/js/bootstrap.bundle.min.js" type="text/javascript"></script>
            <!-- simplebar -->
            <script src="assets/js/simplebar.min.js" type="text/javascript"></script>
            <!-- Icons -->
            <script src="assets/js/feather.min.js" type="text/javascript"></script>
            <!-- Main Js -->
            <script src="assets/js/app.js" type="text/javascript"></script>
            <script type="text/javascript">

                                                async function viewDetail(id) {
                                                    document.getElementById("subject").innerHTML = "";
                                                    const res = await fetch("subjectDetail?id=" + id).then(res => res.json());
                                                    const m = await res;
                                                    document.getElementById("subject").innerHTML = `<li clas s ="d-flex " >
                                                                    <h6>Code:</ h 6>
                                                                    <p class="text-muted ms-2">` + m.subject_code + `</p>
                                                                    </li>
                                                                    <li class="d-flex">
                                                        <h6>Name:</h6>
                                                        <p class="text-muted ms-2">` + m.subject_name + `</p>
                                                            </li>
                                                                    <li class="d-flex">
                                                            <h6>Manager:</h6>
                                                        <p class="text-muted ms-2">` + m.manager + `</p>
                                                    </li>

                                            <li class="d-flex">
                                                    <h6 class="mb-0">Expert:</h6>
                                                    <p class="text-muted ms-2 mb-0">` + m.expert + `</p>
                                                    </li>
                                                                    <li class="d-flex">
                                                        <h6 class="mb-0">Status:</h6>
                                                <p class= "text-m u ted ms-2 mb-0">` + m.status + `</p>
                                              </li>
                                            <li class="d-flex">
                                                <h6 class="mb-0">Description:</h6>
                                                <p class="text-muted ms-2 mb-0">` + m.body + `</p>
                                            </li>`;
                                                }
                                                let orderBy;
                                                let sortBy;
                                                let index;
                                                nextPage(index)
                                                function setSort(od, sb) {
                                                    orderBy = od;
                                                    sortBy = sb;
                                                    sort();
                                                }
                                                async function nextPage(i) {
                                                    index = i;
                                                    var btns = document.getElementsByClassName("page-item");
                                                    for (var i = 0; i < btns.length; i++) {
                                                        btns[i].addEventListener("click", function () {
                                                            var current = document.getElementsByClassName("active");
                                                            current[0].className = current[0].className.replace(" active", "");
                                                            this.className += " active";
                                                        });
                                                    }
                                                    sort();
                                                }
                                                async function sort() {

                                                    var btns = document.getElementsByClassName("fas");
                                                    console.log(btns)
                                                    for (var i = 0; i < btns.length; i++) {
                                                        btns[i].addEventListener("click", function () {
                                                            var current = document.getElementsByClassName("active");
                                                            current[0].className = current[0].className.replace(" active", "");
                                                            this.className += " active";
                                                            console.log("da vao");
                                                        });
                                                    }
                                                    console.log(sortBy, orderBy);
                                                    document.getElementById("bodytable").innerHTML = "";
                                                    if (index == "") {
                                                        index = 1;

                                                    }
                                                    const res = await fetch("sortController?order=" + orderBy + "&sort=" + sortBy + "&index=" + index).then(res => res.json());
                                                    const m = await res;
                                                    console.log(m);
                                                    document.getElementById("bodytable").innerHTML =
                                                            m.map(c => `<tr>
                                                    <th class="p-3">` + c.subject_id + `</th>
                                                    <td class="py-3">
                                                        <a href="#" class="text-dark">
                                                            <div class="d-flex align-items-center">
                                                                <span class="ms-2">` + c.subject_code + `</span>
                                                            </div>
                                                        </a>
                                                    </td>
                                                    <td class="p-3">` + c.subject_name + `</td>
                                                    <td class="p-3">` + c.manager + `</td>
                                                    <td class="p-3">` + c.expert + `</td>
                                                    <td class="p-3" style="color: blue">`
                                                                        + (c.status === 1 ? "Active" : "Deactive") +
                                                                        `
                                                                                                               
                                                        </td>
                                                        <td class="text-end p-3">
                                                        <a  onclick="viewDetail(` + c.subject_id + `)" class="btn btn-icon btn-pills btn-soft-primary" data-bs-toggle="modal" data-bs-target="#viewprofile">
                                                            <i class="uil uil-eye"></i></a>
                                                        <a href="editController?id=` + c.subject_id + `">
                                                            <i class="uil uil-pen"></i></a>
                                                        <a href="addController">
                                                            <i class="fas fa-plus"></i></a>
                                                    </td>
                                                        

                                                </tr>`);
                                                }
                                                sort();


            </script>   
    </body>

</html>