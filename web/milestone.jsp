
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Doctris - Doctor Appointment Booking System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="../../../index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="../assets/images/favicon.ico.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css"/>
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css"/>
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
        <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet" />
        <style>
            .filter-group{
                margin-right: 20px;
            }

        </style>
    </head>

    <body>
        <!-- Loader -->
        <!-- Loader -->
        <jsp:include page="Loader.jsp"></jsp:include>
            <!-- Loader -->

            <div class="page-wrapper doctris-theme toggled">
            <jsp:include page="AdSideBar.jsp"></jsp:include>
                <!-- sidebar-wrapper  -->

                <!-- Start Page Content -->
                <main class="page-content bg-light">

                    <div class="top-header">
                        <div class="header-bar d-flex justify-content-between border-bottom">

                        <jsp:include page="AdHeader.jsp"></jsp:include>

                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="layout-specing">
                            <div class="d-md-flex justify-content-between">
                                <h5 class="mb-0" style="color: orange;font: 30px">MileStone List</h5>
                                <nav aria-label="breadcrumb" class="d-inline-block mt-4 mt-sm-0">
                                    <ul class="breadcrumb bg-transparent rounded mb-0 p-0">
                                        <li class="breadcrumb-item"><a style="padding: 15px 20px;background-color: orangered;color: white" href="addnewMilestone"> <i style="color: blue;width:  20px" class="fas fa-plus"></i> ADD NEW MILESTONE</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <form style="padding-top: 30px"action="searchmilestone" method="Post" id="myForm">
                                <div class="table-filter" style="display: flex">
                                <div class="filter-group">
                                        <input name="title" value="${title}" type="text" class="form-control">
                                </div>
                                <div class="filter-group">
                                    <input name="fromdate" value="${fromdate}" type="date" class="form-control">
                                </div><!-- comment --> 
                                <div class="filter-group">
                                    <input name="todate" value="${todate}" type="date" class="form-control">
                                </div>
                                <div class="filter-group">
                                    <select class="form-select" name="filterStatus">
                                        <option <c:if test="${status eq null || status eq null }">selected</c:if> value="2">All</option>
                                        <option <c:if test="${status==1}">selected</c:if> value="1">Active</option>
                                        <option <c:if test="${status==0}">selected</c:if> value="0">Inactive</option>
                                    </select>
                                </div>
                                <div class="filter-group">
                                    <select class="form-select" name="class">
                                        <c:forEach items="${listC}" var="c">
                                            <option <c:if test="${classcode eq c.class_code}">selected</c:if>  value="${c.class_code}" >${c.class_code}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-icon btn-pills btn-info float-end ms-3"><i class="uil uil-search"></i></button>
                            </div>
                        </form>

                        <div class="row">
                            <div class="col-12 mt-4">
                                <div class="table-responsive shadow rounded">
                                    <table class="table table-center bg-white mb-0">
                                        <thead>
                                            <tr>
                                                <th class="border-bottom p-3" style="min-width: 50px;">Id</th>
                                                <th class="border-bottom p-3" style="min-width: 180px;">Title Assignment</th>
                                                <th class="border-bottom p-3">Class code</th>
                                                <th class="border-bottom p-3">From Date</th>
                                                <th class="border-bottom p-3">To Date</th>
                                                <th class="border-bottom p-3">Status</th>
                                                <th class="border-bottom p-3" style="min-width: 100px;"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${listMilestone}" var="o">
                                                <tr>
                                                    <th class="p-3">${o.milestone_id}</th>
                                                    <td class="py-3">
                                                        <a href="#" class="text-dark">
                                                            <div class="d-flex align-items-center">
                                                                <span class="ms-2">${o.titleAss}</span>
                                                            </div>
                                                        </a>
                                                    </td>
                                                    <td class="p-3">${o.class_code}</td>
                                                    <td class="p-3">${o.from_date}</td>
                                                    <td class="p-3">${o.to_date}</td>

                                                    <td class="p-3">
                                                        <c:if test="${o.status==1}"><span id="s${o.milestone_id}" class="badge bg-soft-success">Active</span></c:if>
                                                        <c:if test="${o.status==0}"><span id="s${o.milestone_id}" class="badge bg-soft-success">DeActive</span></c:if>

                                                        </td>
                                                        <td class="text-end p-3">
                                                            <a href="#" class="btn btn-icon btn-pills btn-soft-primary" data-bs-toggle="modal" data-bs-target="#viewprofile" onclick="viewDetail(${o.milestone_id})"><i class="uil uil-eye"></i></a>
                                                        <a href="editmilestone?milestoneId=${o.milestone_id}" class="btn btn-icon btn-pills btn-soft-success" data-bs-target="#editprofile"><i class="uil uil-pen"></i></a>
                                                        <a class="btn btn-icon btn-pills btn-soft-success" data-bs-target="#editprofile"><i  onclick="changeStatus(${o.milestone_id})"  class="fa-solid fa-recycle"></i></a>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--end row-->

                        <div class="row text-center">
                            <!-- PAGINATION START -->
                            <div class="col-12 mt-4">
                                <div class="d-md-flex align-items-center text-center justify-content-between">
                                    <ul class="pagination justify-content-center mb-0 mt-3 mt-sm-0">
                                        <!--<li class="page-item active"><a class="page-link" href="javascript:void(0)">1</a></li>-->
                                        <c:forEach begin="1" end="${endP}" var="i" >
                                            <li class="page-item <c:if test="${i eq index}">active</c:if> "><a class="page-link" href="milestone?index=${i}" >${i}</a></li>
                                            </c:forEach>
                                    </ul>
                                </div>
                            </div><!--end col-->
                            <!-- PAGINATION END -->
                        </div><!--end row-->
                    </div>
                </div><!--end container-->

                <!-- Footer Start -->
                <jsp:include page="AdFooter.jsp"></jsp:include>
                <!-- End -->
            </main>
            <!--End page-content" -->
        </div>

        <!-- Profile Settings End -->

        <!-- Profile Start -->
        <div class="modal fade" id="viewprofile" tabindex="-1" aria-labelledby="exampleModalLabel1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header border-bottom p-3">
                        <h5 class="modal-title" id="exampleModalLabel1">MileStone Detail</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-3 pt-4">
                        <div class="d-flex align-items-center">
                        </div>
                        <ul class="list-unstyled mb-0 d-md-flex justify-content-between mt-4">
                            <li>
                                <ul id="milestone" class="list-unstyled mb-0">
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Profile End -->
        <!-- Modal end -->

        <!-- javascript -->
        <script src="assets/js/bootstrap.bundle.min.js" type="text/javascript"></script>
        <!-- simplebar -->
        <script src="assets/js/simplebar.min.js" type="text/javascript"></script>
        <!-- Icons -->
        <script src="assets/js/feather.min.js" type="text/javascript"></script>
        <!-- Main Js -->
        <script src="assets/js/app.js" type="text/javascript"></script>
        <script type="text/javascript">
                                        async function viewDetail(id) {
                                            document.getElementById("milestone").innerHTML = "";
                                            const res = await fetch("milestoneDetail?id=" + id).then(res => res.json());
                                            const m = await res;
                                            console.log(m);
                                            document.getElementById("milestone").innerHTML = `<li clas s ="d-flex " >
                                                                    <h6>Title Assignment: </ h 6>
                                                                    <p class="text-muted ms-2">` + m.titleAss + `</p>
                                                                    </li>
                                                                    <li class="d-flex">
                                                        <h6>Class code:</h6>
                                                        <p class="text-muted ms-2">` + m.class_code + `</p>
                                                            </li>
                                                                    <li class="d-flex">
                                                            <h6>From Date:</h6>
                                                        <p class="text-muted ms-2">` + m.from_date + `</p>
                                                    </li>

                                            <li class="d-flex">
                                                    <h6 class="mb-0">To Date:</h6>
                                                    <p class="text-muted ms-2 mb-0">` + m.to_date + `</p>
                                                    </li>
                                            <li class="d-flex">
                                                    <h6 class="mb-0">Ass_body:</h6>
                                                    <p class="text-muted ms-2 mb-0">` + m.ass_body + `</p>
                                                    </li>
                                                                    <li class="d-flex">
                                                        <h6 class="mb-0">Status:</h6>
                                                       <p class="text-muted ms-2 mb-0">` + (m.status === 1 ? "Active" : "Deactive") + `</p>
                                              </li>
                                            <li class="d-flex">
                                                <h6 class="mb-0">Description:</h6>
                                                <p class="text-muted ms-2 mb-0">` + m.description + `</p>
                                            </li>`;
                                        }
                                        async function changeStatus(idmile) {
                                            let text = "Do you sure status Change ?";
                                            if (confirm(text) == true) {
                                                const res = await fetch("searchmilestone?idmile=" + idmile, {method: 'get'}).then(res => res.text());
//                                                alert("Change status Successful");
console.log("da ");
                                                document.getElementById('s'+idmile).innerHTML= res;
                                            } else {
                                                text = "You canceled!";
                                            }
                                        }
        </script>
    </body>

</html>