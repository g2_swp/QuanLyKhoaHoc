<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Setting List</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="../../../index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="course/images/logo.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <style>
            i.fa-regular {
                font-family: fontawesome !important;
            }
            .logo span {
                font-family: 'Open Sans', sans-serif;
                font-size: 30px;
                font-weight: 900;
                vertical-align: middle;
                text-transform: uppercase;
                margin-left: 3px;
            }
            .table-title .btn span {
                float: left;
            }
            .table-filter .filter-group {
                float: right;
                margin-left: 15px;
            }

            .table-filter label {
                font-size: 18px;
                margin-left: 10px;
            }
            .table-filter select, .table-filter input {
                display: inline-block;
                margin-left: 5px;
            }
            .table-filter input {
                width: 200px;
                display: inline-block;
            }
            .filter-group select.form-control, .filter-group select.form-select{
                width: 145px;
            }
            .filter-icon {
                float: right;
            }
        </style>

    </head>

    <body class="bg-dark">

        <div class="page-wrapper doctris-theme">
            <!-- SideBar -->
            <jsp:include page="AdSideBar.jsp"></jsp:include>
                <!-- SideBar  -->

                
                <!-- Start Page Content -->
                <main class="page-content bg-light">

                    <!-- Header  -->
                <jsp:include page="AdHeader.jsp"></jsp:include>
                    <!-- Header  -->

                    <!-- container -->
                    <div class="container-fluid">
                        <div class="layout-specing">

                            <div class="row bg-warning text-white p-4">
                                <div class="col-md-3 ps-4">
                                    <h2 class="mb-0"><b class="text-dark">Setting List</b></h2>
                                </div>
                                <div class="col-md-9 mt-2">
                                    <form action="SettingList" method="Post">
                                        <div class="table-filter">
                                            <div class="filter-group">
                                                <input type="hidden" name="operation" value="wiew_add_setting_page" />
                                                <button type="submit" class="btn btn-icon btn-pills btn-info float-end ms-3"><i class="fa-solid fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    <form action="SettingList" method="Post">
                                        <input type="hidden" name="operation" value="search" />
                                        <div class="table-filter">
                                            <button type="submit" class="btn btn-icon btn-pills btn-info float-end ms-3"><i class="uil uil-search"></i></button>
                                            <div class="filter-group">
                                                <label>Title</label>
                                                <input name="findTitle" value="${requestScope.findTitle}" type="text" class="form-control">
                                        </div>
                                        <div class="filter-group">
                                            <label>Status</label>
                                            <select class="form-select" name="filterStatus">
                                                <option value="2">All</option>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                        <div class="filter-group">
                                            <label>Setting Type</label>
                                            <select class="form-select" name="filterType">
                                                <option value="0">All</option>
                                                <c:forEach items="${sessionScope.setting_types}" var="c">
                                                    <option value="${c.setting_id}" ${c.setting_id == CateID?"selected":""}>${c.setting_title}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <span class="filter-icon text-dark mt-2"><i class="fa fa-filter"></i></span>
                                    </div>
                                </form>
                            </div>
                        </div>






                        <div class="row">
                            <div class="col-12 mt-4">
                                <div class="table-responsive shadow rounded">
                                    <table class="table table-center bg-white mb-0 table-hover">
                                        <thead class="bg-dark">
                                            <tr>
                                                <th class="border-bottom p-3">
                                                    <form action="SettingList" method="POST">
                                                        <input type="hidden" name="sort_column" value="setting_id" />
                                                        <input type="submit" value="Setting ID" style="background: none;border: none; color:white"/>
                                                    </form>
                                                </th>
                                                <th class="border-bottom p-3">
                                                    <form action="SettingList" method="POST">
                                                        <input type="hidden" name="sort_column" value="type_id" />
                                                        <input type="submit" value="Setting Type" style="background: none;border: none; color:white"/>
                                                    </form>
                                                </th>
                                                <th class="border-bottom p-3"style="min-width: 180px;">
                                                    <form action="SettingList" method="POST">
                                                        <input type="hidden" name="sort_column" value="setting_title" />
                                                        <input type="submit" value="Setting Title" style="background: none;border: none; color:white"/>
                                                    </form>
                                                </th>
                                                <th class="border-bottom p-3">
                                                    <form action="SettingList" method="POST">
                                                        <input type="hidden" name="sort_column" value="setting_value" />
                                                        <input type="submit" value="Setting Value" style="background: none;border: none; color:white"/>
                                                    </form>
                                                </th>
                                                <th class="border-bottom p-3">
                                                    <form action="SettingList" method="POST">
                                                        <input type="hidden" name="sort_column" value="display_order" />
                                                        <input type="submit" value="Display Order" style="background: none;border: none; color:white"/>
                                                    </form>
                                                </th>
                                                <th class="border-bottom p-3">
                                                    <form action="SettingList" method="POST">
                                                        <input type="hidden" name="sort_column" value="status" />
                                                        <input type="submit" value="Status" style="background: none;border: none; color:white"/>
                                                    </form>
                                                </th>
                                                <th class="border-bottom p-3" style="color: white">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${requestScope.setting_list}" var="c">
                                                <tr>
                                                    <th class="p-3">${c.setting_id}</th>
                                                    <td class="p-3">
                                                        <span class="ms-2">
                                                            <c:forEach items="${sessionScope.setting_types}" var="t">
                                                                <c:if test="${t.setting_id==c.type_id}">
                                                                    ${t.setting_title}
                                                                </c:if>
                                                            </c:forEach>
                                                        </span>
                                                    </td>
                                                    <td class="p-3">${c.setting_title}</td>
                                                    <td class="p-3">${c.setting_value}</td>
                                                    <td class="p-3">${c.display_order}</td>
                                                    <c:if test="${c.status==0}">
                                                        <td class="p-3"><span class="badge bg-soft-danger">Inactive</span></td>
                                                    </c:if>
                                                    <c:if test="${c.status==1}">
                                                        <td class="p-3"><span class="badge bg-soft-success">Active</span></td>
                                                    </c:if>

                                                    <td class="text-end p-3">
                                                        <a href="SettingController?setting_id=${c.setting_id}" class="d-inline">
                                                            <button class="btn btn-icon btn-pills btn-soft-primary ">
                                                                <i class="uil uil-eye"></i>
                                                            </button>
                                                        </a>
                                                        <c:if test="${c.setting_id>10}">
                                                            <form action="SettingList" method="POST" class="d-inline">
                                                                <input type="hidden" name="setting_id" value="${c.setting_id}" />
                                                                <input type="hidden" name="operation" value="delete_setting" />
                                                                <button type="submit" class="btn btn-icon btn-pills btn-soft-danger "onclick="return confirm('Are you sure?')"><i class="uil uil-trash"></i></button>
                                                            </form>

                                                        </c:if>
                                                    </td>

                                                </tr>
                                            </c:forEach>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--end row-->

                        <c:if test="${requestScope.current_page!=null}">
                            <div class="row text-center">
                                <!-- PAGINATION START -->
                                <div class="col-12 mt-4">
                                    <div class="d-md-flex align-items-center text-center justify-content-between">
                                        <span class="text-muted me-3">Showing ${(requestScope.current_page-1)*10+1} - ${(requestScope.current_page-1)*10+10} out of ${sessionScope.count_setting_number}</span>
                                        <ul class="pagination justify-content-center mb-0 mt-3 mt-sm-0">
                                            <c:forEach var="i" begin="1" end="${sessionScope.setting_page_number}" step="1" >
                                                <c:choose>
                                                    <c:when test="${i==requestScope.current_page}">

                                                        <li class="page-item active">
                                                            <form action="SettingList" method="POST">
                                                                <input type="hidden" name="current_page" value="${i}" />                                                            <button class="page-link" type="submit" name="submit_param" value="submit_value">
                                                                    <c:out value="${i}"/>
                                                                </button>
                                                            </form>
                                                        </li>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <li class="page-item">
                                                            <form action="SettingList" method="POST" class="page-item">
                                                                <input type="hidden" name="current_page" value="${i}" />
                                                                <button class="page-link" type="submit" name="submit_param" value="submit_value">
                                                                    <c:out value="${i}"/>
                                                                </button>
                                                            </form>
                                                        </li>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                </div><!--end col-->
                                <!-- PAGINATION END -->
                            </div><!--end row-->
                        </c:if>
                    </div>
                </div><!--end container-->

                <!-- Footer Start -->
                <jsp:include page="AdFooter.jsp"></jsp:include>
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->

        <!--         Modal start 
                 Profile Settings Start 
                <div class="modal fade" id="editprofile" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header border-bottom p-3">
                                <h5 class="modal-title" id="exampleModalLabel">Profile Settings</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body p-3 pt-4">
                                <div class="row align-items-center">
                                    <div class="col-lg-2 col-md-4">
                                        <img src="assets/images/doctors/01.jpg" class="avatar avatar-md-md rounded-pill shadow mx-auto d-block" alt="">
                                    </div>end col
        
                                    <div class="col-lg-5 col-md-8 text-center text-md-start mt-4 mt-sm-0">
                                        <h6 class="">Upload your picture</h6>
                                        <p class="text-muted mb-0">For best results, use an image at least 256px by 256px in either .jpg or .png format</p>
                                    </div>end col
        
                                    <div class="col-lg-5 col-md-12 text-lg-end text-center mt-4 mt-lg-0">
                                        <a href="#" class="btn btn-primary">Upload</a>
                                        <a href="#" class="btn btn-soft-primary ms-2">Remove</a>
                                    </div>end col
                                </div>end row
        
                                <form class="mt-4">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">First Name</label>
                                                <input name="name" id="name" type="text" class="form-control" placeholder="First Name :">
                                            </div>
                                        </div>end col
        
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Last Name</label>
                                                <input name="name" id="name2" type="text" class="form-control" placeholder="Last Name :">
                                            </div>
                                        </div>end col
        
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Your Email</label>
                                                <input name="email" id="email" type="email" class="form-control" placeholder="Your email :">
                                            </div> 
                                        </div>end col
        
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Phone no.</label>
                                                <input name="number" id="number" type="text" class="form-control" placeholder="Phone no. :">
                                            </div>                                                                               
                                        </div>end col
        
                                        <div class="col-md-12">
                                            <div class="mb-3">
                                                <label class="form-label">Your Bio Here</label>
                                                <textarea name="comments" id="comments" rows="4" class="form-control" placeholder="Bio :"></textarea>
                                            </div>
                                        </div>
                                    </div>end row
        
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="submit" id="submit" name="send" class="btn btn-primary" value="Save changes">
                                        </div>end col
                                    </div>end row
                                </form>end form
                            </div>
                        </div>
                    </div>
                </div>
                 Profile Settings End 
        
                 Profile Start 
                <div class="modal fade" id="viewprofile" tabindex="-1" aria-labelledby="exampleModalLabel1" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header border-bottom p-3">
                                <h5 class="modal-title" id="exampleModalLabel1">Profile</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body p-3 pt-4">
                                <div class="d-flex align-items-center">
                                    <img src="assets/images/client/01.jpg" class="avatar avatar-small rounded-pill" alt="">
                                    <h5 class="mb-0 ms-3">Howard Tanner</h5>
                                </div>
                                <ul class="list-unstyled mb-0 d-md-flex justify-content-between mt-4">
                                    <li>
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-flex">
                                                <h6>Age:</h6>
                                                <p class="text-muted ms-2">25 year old</p>
                                            </li>
        
                                            <li class="d-flex">
                                                <h6>Gender:</h6>
                                                <p class="text-muted ms-2">Male</p>
                                            </li>
        
                                            <li class="d-flex">
                                                <h6 class="mb-0">Department:</h6>
                                                <p class="text-muted ms-2 mb-0">Cardiology</p>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-flex">
                                                <h6>Date:</h6>
                                                <p class="text-muted ms-2">20th Dec 2020</p>
                                            </li>
        
                                            <li class="d-flex">
                                                <h6>Time:</h6>
                                                <p class="text-muted ms-2">11:00 AM</p>
                                            </li>
        
                                            <li class="d-flex">
                                                <h6 class="mb-0">Doctor:</h6>
                                                <p class="text-muted ms-2 mb-0">Dr. Calvin Carlo</p>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                 Profile End 
                 Modal end -->

        <!-- javascript -->
        <script src="assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="assets/js/simplebar.min.js"></script>
        <!-- Icons -->
        <script src="assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="assets/js/app.js"></script>

    </body>

</html>