<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Register User</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="../../../index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="course/images/logo.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <style>
            i.fa-regular {
                font-family: fontawesome !important;
            }
            .logo span {
                font-family: 'Open Sans', sans-serif;
                font-size: 30px;
                font-weight: 900;
                vertical-align: middle;
                text-transform: uppercase;
                margin-left: 3px;
            }
        </style>

    </head>

    <body class="bg-dark">

        <div class="page-wrapper doctris-theme">
            <!-- SideBar -->
            <jsp:include page="AdSideBar.jsp"></jsp:include>
                <!-- SideBar  -->

                <!-- Start Page Content -->
                <main class="page-content bg-light">


                    <!-- Header  -->
                <jsp:include page="AdHeader.jsp"></jsp:include>
                    <!-- Header  -->
                    <section class="bg-dashboard">
                        <div class="container">
                            <div class="rounded shadow mt-4">
                                <div class="p-4 border-bottom">
                                    <h5 class="mb-0">Register User:</h5>
                                </div>

                                <div class="p-4">
                                    <form action="UserController" method="post">
                                        <input type="hidden" name="operation" value="register" />
                                        <div class="row">
                                            
                                        <input name="user_id" id="name" type="hidden" class="form-control"  placeholder="Setting ID:" value="${requestScope.user_id}" disable>


                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">User Name*</label>
                                                <input name="user_name" id="checkInput" pattern="^[a-zA-Z0-9 {1,50}]*$" type="text" class="form-control" placeholder="User Name:"value="" required="">
                                            </div> 
                                        </div><!--end col-->



                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Full Name*</label>
                                                <input name="full_name" id="checkInput" pattern="^[a-zA-Z {0,50}]*$" type="text" class="form-control" placeholder="Full Name:" value="">
                                            </div>                                                                               
                                        </div><!--end col-->
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Email</label>
                                                <input name="email" type="email" class="form-control" placeholder="Email:" value="">
                                            </div>                                                                               
                                        </div><!--end col-->
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Mobile</label>
                                                <input name="mobile" id="myform_phone" pattern="[0-9]{10}" type="tel" class="form-control" placeholder="Mobile:" value="" required>
                                            </div>                                                                               
                                        </div><!--end col-->
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Password</label>
                                                <input name="password" id="password" type="password" class="form-control" placeholder="Password:" value=""onChange="onChange()">
                                            </div>                                                                               
                                        </div><!--end col-->
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Confirm Password</label>
                                                <input name="password" id="confirm_password" type="password" class="form-control" placeholder="Confirm Password:" value="" onChange="onChange()">
                                            </div>                                                                               
                                        </div><!--end col-->








                                        
                                        <input type="hidden" name="status" value="1" />

                                        <input type="hidden" name="note" value="" />
                                    </div><!--end row-->
                                    <div class="col-md-12">
                                        <label class="form-text">(*)  Just input alphabet characters and space; max input lenght is 50.</label>
                                        <br>
                                        <label class="form-text">(**) Just input alphabet characters and space; max input lenght is 250.</label>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-sm-12">
                                            <input type="submit" id="submit" name="send" class="btn btn-primary" value="Register" onclick="return confirm('Are you sure?')" >
                                        </div><!--end col-->
                                    </div><!--end row-->

                                </form><!--end form--> 
                            </div>
                        </div>
                    </div><!--end container-->
                </section><!--end section-->

                <!-- Footer Start -->
                <jsp:include page="AdFooter.jsp"></jsp:include>
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->

        <!-- javascript -->
        <script src="assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="assets/js/simplebar.min.js"></script>
        <!-- Icons -->
        <script src="assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="assets/js/app.js"></script>
        <script>
                                                var check_input = document.getElementById("checkInput");

                                                check_input.addEventListener('input', () => {
                                                    check_input.setCustomValidity('Just input alphabet characters and max input lenght is 50');
                                                    check_input.checkValidity();
                                                });

                                                check_input.addEventListener('invalid', () => {
                                                if (check_input.value === '') {
                                                check_input.setCustomValidity('Please fill this field');
                                                } else {
                                                check_input.setCustomValidity('Just input alphabet characters and max input lenght is 50');
                                                {
                                                });
        </script>
        <script>
                    var phone_input = document.getElementById("myform_phone");
                    phone_input.addEventListener('input', () => {
                    phone_input.setCustomValidity('');
                            phone_input.checkValidity();
                    });
                    phone_input.addEventListener('invalid', () => {
                    if (phone_input.value === '') {
                    phone_input.setCustomValidity('Enter phone number!');
                    } else {
                    phone_input.setCustomValidity('Enter phone number in this format: 0123-456-789');
                    }
                    });
        </script>
        <script>
                    function onChange() {
                    const password = document.getElementById("password");
                            const confirm = document.getElementById("confirm_password");
                            if (confirm.value === password.value) {
                    confirm.setCustomValidity('');
                    } else {
                    confirm.setCustomValidity('Passwords do not match');
                    }
                    }
        </script>
    </body>

</html>