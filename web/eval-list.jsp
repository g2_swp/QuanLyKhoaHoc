<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Eval-criteria List</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="../../../index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="course/images/logo.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <!-- Table Sort Css -->
        <link href="assets/css/tablesort.css" rel="stylesheet" type="text/css" />
        
        <style>
            i.fa-regular {
                font-family: fontawesome !important;
            }
            .logo span {
                font-family: 'Open Sans', sans-serif;
                font-size: 30px;
                font-weight: 900;
                vertical-align: middle;
                text-transform: uppercase;
                margin-left: 3px;
            }
            .table-title .btn span {
                float: left;
            }
            .table-filter .filter-group {
                float: right;
                margin-left: 15px;
            }

            .table-filter label {
                font-size: 18px;
                margin-left: 10px;
            }
            .table-filter select, .table-filter input {
                display: inline-block;
                margin-left: 5px;
            }
            .table-filter input {
                width: 200px;
                display: inline-block;
            }
            .filter-group select.form-control, .filter-group select.form-select{
                width: 145px;
            }
            .filter-icon {
                float: right;
            }
        </style>

    </head>

    <body class="bg-dark">
        
        <div class="page-wrapper doctris-theme">
            <!-- SideBar -->
            <jsp:include page="AdSideBar.jsp"></jsp:include>
                <!-- SideBar  -->

                <!-- Start Page Content -->
                <main class="page-content bg-light">

                    <!-- Header  -->
                <jsp:include page="AdHeader.jsp"></jsp:include>
                    <!-- Header  -->

                    <!-- container -->
                    <div class="container-fluid">
                        <div class="layout-specing">
                            <div class="row bg-warning text-white p-4">
                                <div class="col-md-3 ps-4">
                                    <h2 class="mb-0"><b class="text-dark">Eval-criteria</b> List</h2>
                                </div>
                                <div class="col-md-9 mt-2">
                                    <form action="EvalList" method="Post">
                                        <div class="table-filter">
                                            <button type="submit" class="btn btn-icon btn-pills btn-info float-end ms-3"><i class="uil uil-search"></i></button>
<!--                                            <div class="filter-group">
                                                <label>Search</label>
                                                <input name="searchName" value="${findName}" type="text" class="form-control">
                                            </div>-->
                                            <div class="filter-group">
                                                <label>Assignment</label>
                                                <select class="form-select" name="ass_id">
                                                    <option value="">All Assignment</option>
                                                <c:forEach items="${listass}" var="c">
                                                    <option value="${c.ass_id}" ${c.ass_id == AssID?"selected":""}>${c.title}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <span class="filter-icon text-dark mt-2"><i class="fa fa-filter"></i></span>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive shadow rounded">
                                    <table class="table table-sortable table-center bg-white mb-0">
                                        <thead>
                                            <tr>
                                                <th class="border-bottom p-3" style="min-width: 50px;">Id</th>
                                                <th class="border-bottom p-3" style="min-width: 180px;">Assignment</th>
                                                <th class="border-bottom p-3">Milestone</th>
                                                <th class="border-bottom p-3">Criteria Name</th>
                                                <th class="border-bottom p-3">Team Eval</th>
                                                <th class="border-bottom p-3">Weight</th>
                                                <th class="border-bottom p-3">Max LOC</th>
                                                <th class="border-bottom p-3">Status</th>
                                                <th class="border-bottom p-3" style="min-width: 50px;"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${sessionScope.listEval}" var="c">
                                                <tr>
                                                    <td class="p-3">${c.criteria_id}</td>
                                                    <td class="py-3"><span class="ms-2">${c.assignment.title}</span></td>
                                                    <td class="p-3">${c.milestone_id}</td>
                                                    <td class="p-3">${c.criteria_name}</td>
                                                    <td class="p-3">${c.is_team_eval}</td>
                                                    <td class="p-3">${c.eval_weight}</td>
                                                    <td class="p-3">${c.max_loc}</td>
                                                    <c:if test="${c.status==0}">
                                                        <td class="p1-3"><span class="badge bg-soft-danger">Processing</span></td>
                                                    </c:if>
                                                    <c:if test="${c.status==1}">
                                                        <td class="p1-3"><span class="badge bg-soft-success">Complete</span></td>
                                                    </c:if>
                                                    <td class="text-center p-3">
                                                        <a href="EvalDetail?criteria_id=${c.criteria_id}" class="btn btn-icon btn-pills btn-soft-success"><i class="uil uil-pen"></i></a>
                                                    </td>
                                                </tr>
                                        </c:forEach>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--end row-->

                        <div class="row text-center">
                            <!-- PAGINATION START -->
                            <div class="col-12 mt-4">
                                <div class="d-md-flex align-items-center text-center justify-content-between">
                                    <span class="text-muted me-3">Showing 1 - 10 out of 50</span>
                                    <ul class="pagination justify-content-center mb-0 mt-3 mt-sm-0">
                                        <c:if test="${page!=1}">
                                            <li class="page-item"><a class="page-link" href="ContactList?page=${1}" aria-label="Previous">First</a></li>
                                            <li class="page-item"><a class="page-link" href="ContactList?page=${page-1}" aria-label="Previous">Prev</a></li>
                                            </c:if>
                                            <c:forEach begin="${1}" end="${requestScope.numP}" var="p">
                                            <li class="page-item ${p==page? "active":""}"><a class="page-link" href="ContactList?page=${p}">${p}</a></li>
                                            </c:forEach>
                                            <c:if test="${page!=numP}">
                                            <li class="page-item"><a class="page-link" href="ContactList?page=${page+1}" aria-label="Next">Next</a></li>
                                            <li class="page-item"><a class="page-link" href="ContactList?page=${numP}" aria-label="Previous">Last</a></li>
                                        </c:if>
                                    </ul>
                                </div>
                            </div><!--end col-->
                            <!-- PAGINATION END -->
                        </div><!--end row-->
                    </div>
                </div><!--end container-->

                <!-- Footer Start -->
                <jsp:include page="AdFooter.jsp"></jsp:include>
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->


        <!-- javascript -->
        <script src="assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="assets/js/simplebar.min.js"></script>
        <!-- Icons -->
        <script src="assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="assets/js/app.js"></script>
        <!-- Table Sort Js -->
        <script src="assets/js/tablesort.js"></script>

    </body>

</html>