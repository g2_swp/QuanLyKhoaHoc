<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Course</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="../../../index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="course/images/logo.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <!-- SLIDER -->
        <link rel="stylesheet" type="text/css" href="course/plugins/OwlCarousel2-2.2.1/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="course/plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
        <link rel="stylesheet" type="ext/css" href="course/plugins/OwlCarousel2-2.2.1/animate.css">
        <link rel="stylesheet" type="text/css" href="course/styles/main_styles.css">  <!--Cause Error!!!!!-->
        <link rel="stylesheet" type="text/css" href="course/styles/responsive.css">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <style>
            i.fa-regular {
                font-family: fontawesome !important;
            }
        </style>
    </head>
    <body>
        <!-- Header -->
        <jsp:include page="Header.jsp"></jsp:include>

        <!-- Home -->

        <div class="home">

            <!-- Hero Slider -->
            <div class="hero_slider_container">
                <div class="hero_slider owl-carousel">

                    <!-- Hero Slide -->
                    <div class="hero_slide">
                        <div class="hero_slide_background" style="background-image:url(course/images/slider_background.jpg)"></div>
                        <div class="hero_slide_container d-flex flex-column align-items-center justify-content-center">
                            <div class="hero_slide_content text-center">
                                <h1 data-animation-in="fadeInUp" data-animation-out="animate-out fadeOut">Welcome To <span>Course</span></h1>
                            </div>
                        </div>
                    </div>

                    <!-- Hero Slide -->
                    <div class="hero_slide">
                        <div class="hero_slide_background" style="background-image:url(course/images/slider_background.jpg)"></div>
                        <div class="hero_slide_container d-flex flex-column align-items-center justify-content-center">
                            <div class="hero_slide_content text-center">
                                <h1 data-animation-in="fadeInUp" data-animation-out="animate-out fadeOut">Get your <span>Education</span> today!</h1>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="hero_boxes">
            <div class="hero_boxes_inner">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-4 hero_box_col">
                            <div class="hero_box d-flex flex-row align-items-center justify-content-start">
                                <img src="course/images/earth-globe.svg" class="svg" alt="">
                                <div class="hero_box_content">
                                    <h2 class="hero_box_title">Online Courses</h2>
                                    <a href="courses.html" class="hero_box_link">view more</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 hero_box_col">
                            <div class="hero_box d-flex flex-row align-items-center justify-content-start">
                                <img src="course/images/books.svg" class="svg" alt="">
                                <div class="hero_box_content">
                                    <h2 class="hero_box_title">Our Library</h2>
                                    <a href="courses.html" class="hero_box_link">view more</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 hero_box_col">
                            <div class="hero_box d-flex flex-row align-items-center justify-content-start">
                                <img src="course/images/professor.svg" class="svg" alt="">
                                <div class="hero_box_content">
                                    <h2 class="hero_box_title">Our Teachers</h2>
                                    <a href="teachers.html" class="hero_box_link">view more</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Popular -->

        <div class="popular page_section">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="section_title text-center">
                            <h1>Popular Courses</h1>
                        </div>
                    </div>
                </div>

                <div class="row course_boxes">

                    <!-- Popular Course Item -->
                    <div class="col-lg-4 course_box">
                        <div class="card">
                            <img class="card-img-top" src="course/images/course_1.jpg" alt="https://unsplash.com/@kellybrito">
                            <div class="card-body text-center">
                                <div class="card-title"><a href="courses.html">A complete guide to design</a></div>
                                <div class="card-text">Adobe Guide, Layes, Smart Objects etc...</div>
                            </div>
                            <div class="price_box d-flex flex-row align-items-center">
                                <div class="course_author_image">
                                    <img src="course/images/author.jpg" alt="https://unsplash.com/@mehdizadeh">
                                </div>
                                <div class="course_author_name">Michael Smith, <span>Author</span></div>
                                <div class="course_price d-flex flex-column align-items-center justify-content-center"><span>$29</span></div>
                            </div>
                        </div>
                    </div>

                    <!-- Popular Course Item -->
                    <div class="col-lg-4 course_box">
                        <div class="card">
                            <img class="card-img-top" src="course/images/course_2.jpg" alt="https://unsplash.com/@cikstefan">
                            <div class="card-body text-center">
                                <div class="card-title"><a href="courses.html">Beginners guide to HTML</a></div>
                                <div class="card-text">Adobe Guide, Layes, Smart Objects etc...</div>
                            </div>
                            <div class="price_box d-flex flex-row align-items-center">
                                <div class="course_author_image">
                                    <img src="course/images/author.jpg" alt="https://unsplash.com/@mehdizadeh">
                                </div>
                                <div class="course_author_name">Michael Smith, <span>Author</span></div>
                                <div class="course_price d-flex flex-column align-items-center justify-content-center"><span>$29</span></div>
                            </div>
                        </div>
                    </div>

                    <!-- Popular Course Item -->
                    <div class="col-lg-4 course_box">
                        <div class="card">
                            <img class="card-img-top" src="course/images/course_3.jpg" alt="https://unsplash.com/@dsmacinnes">
                            <div class="card-body text-center">
                                <div class="card-title"><a href="courses.html">Advanced Photoshop</a></div>
                                <div class="card-text">Adobe Guide, Layes, Smart Objects etc...</div>
                            </div>
                            <div class="price_box d-flex flex-row align-items-center">
                                <div class="course_author_image">
                                    <img src="course/images/author.jpg" alt="https://unsplash.com/@mehdizadeh">
                                </div>
                                <div class="course_author_name">Michael Smith, <span>Author</span></div>
                                <div class="course_price d-flex flex-column align-items-center justify-content-center"><span>$29</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>		
        </div>

        <!-- Register -->

        <div class="register">

            <div class="container-fluid">

                <div class="row row-eq-height">
                    <div class="col-lg-6 nopadding">

                        <!-- Register -->

                        <div class="register_section d-flex flex-column align-items-center justify-content-center">
                            <div class="register_content text-center">
                                <h1 class="register_title">Register now and get a discount <span>50%</span> discount until 1 January</h1>
                                <p class="register_text">In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum. Aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempo.</p>
                                <div class="button button_1 register_button mx-auto trans_200"><a href="#">register now</a></div>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-6 nopadding">

                        <!-- Search -->

                        <div class="search_section d-flex flex-column align-items-center justify-content-center">
                            <div class="search_background" style="background-image:url(course/images/search_background.jpg);"></div>
                            <div class="search_content text-center">
                                <h1 class="search_title">Search for your course</h1>
                                <form id="search_form" class="search_form" action="post">
                                    <input id="search_form_name" class="input_field search_form_name" type="text" placeholder="Course Name" required="required" data-error="Course name is required.">
                                    <input id="search_form_category" class="input_field search_form_category" type="text" placeholder="Category">
                                    <input id="search_form_degree" class="input_field search_form_degree" type="text" placeholder="Degree">
                                    <button id="search_submit_button" type="submit" class="search_submit_button trans_200" value="Submit">search course</button>
                                </form>
                            </div> 
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Services -->

        <div class="services page_section">

            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="section_title text-center">
                            <h1>Our Services</h1>
                        </div>
                    </div>
                </div>

                <div class="row services_row">

                    <div class="col-lg-4 service_item text-left d-flex flex-column align-items-start justify-content-start">
                        <div class="icon_container d-flex flex-column justify-content-end">
                            <img src="course/images/earth-globe.svg" alt="">
                        </div>
                        <h3>Online Courses</h3>
                        <p>In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum.</p>
                    </div>

                    <div class="col-lg-4 service_item text-left d-flex flex-column align-items-start justify-content-start">
                        <div class="icon_container d-flex flex-column justify-content-end">
                            <img src="course/images/exam.svg" alt="">
                        </div>
                        <h3>Indoor Courses</h3>
                        <p>In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum.</p>
                    </div>

                    <div class="col-lg-4 service_item text-left d-flex flex-column align-items-start justify-content-start">
                        <div class="icon_container d-flex flex-column justify-content-end">
                            <img src="course/images/books.svg" alt="">
                        </div>
                        <h3>Amazing Library</h3>
                        <p>In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum.</p>
                    </div>

                    <div class="col-lg-4 service_item text-left d-flex flex-column align-items-start justify-content-start">
                        <div class="icon_container d-flex flex-column justify-content-end">
                            <img src="course/images/professor.svg" alt="">
                        </div>
                        <h3>Exceptional Professors</h3>
                        <p>In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum.</p>
                    </div>

                    <div class="col-lg-4 service_item text-left d-flex flex-column align-items-start justify-content-start">
                        <div class="icon_container d-flex flex-column justify-content-end">
                            <img src="course/images/blackboard.svg" alt="">
                        </div>
                        <h3>Top Programs</h3>
                        <p>In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum.</p>
                    </div>

                    <div class="col-lg-4 service_item text-left d-flex flex-column align-items-start justify-content-start">
                        <div class="icon_container d-flex flex-column justify-content-end">
                            <img src="course/images/mortarboard.svg" alt="">
                        </div>
                        <h3>Graduate Diploma</h3>
                        <p>In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum.</p>
                    </div>

                </div>
            </div>
        </div>

        <!-- Testimonials -->

        <div class="testimonials page_section">
            <!-- <div class="testimonials_background" style="background-image:url(course/images/testimonials_background.jpg)"></div> -->
            <div class="testimonials_background_container prlx_parent">
                <div class="testimonials_background prlx" style="background-image:url(course/images/testimonials_background.jpg)"></div>
            </div>
            <div class="container">

                <div class="row">
                    <div class="col">
                        <div class="section_title text-center">
                            <h1>What our students say</h1>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-10 offset-lg-1">

                        <div class="testimonials_slider_container">

                            <!-- Testimonials Slider -->
                            <div class="owl-carousel owl-theme testimonials_slider">

                                <!-- Testimonials Item -->
                                <div class="owl-item">
                                    <div class="testimonials_item text-center">
                                        <div class="quote">?</div>
                                        <p class="testimonials_text">In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum.In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum.</p>
                                        <div class="testimonial_user">
                                            <div class="testimonial_image mx-auto">
                                                <img src="course/images/testimonials_user.jpg" alt="">
                                            </div>
                                            <div class="testimonial_name">james cooper</div>
                                            <div class="testimonial_title">Graduate Student</div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Testimonials Item -->
                                <div class="owl-item">
                                    <div class="testimonials_item text-center">
                                        <div class="quote">?</div>
                                        <p class="testimonials_text">In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum.In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum.</p>
                                        <div class="testimonial_user">
                                            <div class="testimonial_image mx-auto">
                                                <img src="course/images/testimonials_user.jpg" alt="">
                                            </div>
                                            <div class="testimonial_name">james cooper</div>
                                            <div class="testimonial_title">Graduate Student</div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Testimonials Item -->
                                <div class="owl-item">
                                    <div class="testimonials_item text-center">
                                        <div class="quote">?</div>
                                        <p class="testimonials_text">In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum.In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum.</p>
                                        <div class="testimonial_user">
                                            <div class="testimonial_image mx-auto">
                                                <img src="course/images/testimonials_user.jpg" alt="">
                                            </div>
                                            <div class="testimonial_name">james cooper</div>
                                            <div class="testimonial_title">Graduate Student</div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Events -->

        <div class="events page_section">
            <div class="container">

                <div class="row">
                    <div class="col">
                        <div class="section_title text-center">
                            <h1>Upcoming Events</h1>
                        </div>
                    </div>
                </div>

                <div class="event_items">

                    <!-- Event Item -->
                    <div class="row event_item">
                        <div class="col">
                            <div class="row d-flex flex-row align-items-end">

                                <div class="col-lg-2 order-lg-1 order-2">
                                    <div class="event_date d-flex flex-column align-items-center justify-content-center">
                                        <div class="event_day">07</div>
                                        <div class="event_month">January</div>
                                    </div>
                                </div>

                                <div class="col-lg-6 order-lg-2 order-3">
                                    <div class="event_content">
                                        <div class="event_name"><a class="trans_200" href="#">Student Festival</a></div>
                                        <div class="event_location">Grand Central Park</div>
                                        <p>In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor.</p>
                                    </div>
                                </div>

                                <div class="col-lg-4 order-lg-3 order-1">
                                    <div class="event_image">
                                        <img src="course/images/event_1.jpg" alt="https://unsplash.com/@theunsteady5">
                                    </div>
                                </div>

                            </div>	
                        </div>
                    </div>

                    <!-- Event Item -->
                    <div class="row event_item">
                        <div class="col">
                            <div class="row d-flex flex-row align-items-end">

                                <div class="col-lg-2 order-lg-1 order-2">
                                    <div class="event_date d-flex flex-column align-items-center justify-content-center">
                                        <div class="event_day">07</div>
                                        <div class="event_month">January</div>
                                    </div>
                                </div>

                                <div class="col-lg-6 order-lg-2 order-3">
                                    <div class="event_content">
                                        <div class="event_name"><a class="trans_200" href="#">Open day in the Univesrsity campus</a></div>
                                        <div class="event_location">Grand Central Park</div>
                                        <p>In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor.</p>
                                    </div>
                                </div>

                                <div class="col-lg-4 order-lg-3 order-1">
                                    <div class="event_image">
                                        <img src="course/images/event_2.jpg" alt="https://unsplash.com/@claybanks1989">
                                    </div>
                                </div>

                            </div>	
                        </div>
                    </div>

                    <!-- Event Item -->
                    <div class="row event_item">
                        <div class="col">
                            <div class="row d-flex flex-row align-items-end">

                                <div class="col-lg-2 order-lg-1 order-2">
                                    <div class="event_date d-flex flex-column align-items-center justify-content-center">
                                        <div class="event_day">07</div>
                                        <div class="event_month">January</div>
                                    </div>
                                </div>

                                <div class="col-lg-6 order-lg-2 order-3">
                                    <div class="event_content">
                                        <div class="event_name"><a class="trans_200" href="#">Student Graduation Ceremony</a></div>
                                        <div class="event_location">Grand Central Park</div>
                                        <p>In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor.</p>
                                    </div>
                                </div>

                                <div class="col-lg-4 order-lg-3 order-1">
                                    <div class="event_image">
                                        <img src="course/images/event_3.jpg" alt="https://unsplash.com/@juanmramosjr">
                                    </div>
                                </div>

                            </div>	
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <!-- Footer -->
        <jsp:include page="Footer.jsp"></jsp:include>

        <!-- Back to top -->
        <a href="#" onclick="topFunction()" id="back-to-top" class="btn btn-icon btn-pills btn-warning back-to-top"><i data-feather="arrow-up" class="icons"></i></a>
        <!-- Back to top -->


        <!-- javascript -->
        <script src="assets/js/bootstrap.bundle.min.js"></script>
        <!-- SLIDER -->
        <script src="assets/js/tiny-slider.js"></script>
        <script src="assets/js/tiny-slider-init.js"></script>
        <!-- Counter -->
        <script src="assets/js/counter.init.js"></script>
        <!-- Icons -->
        <script src="assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="assets/js/app.js"></script>

        <script src="course/js/jquery-3.2.1.min.js"></script>
        <script src="course/plugins/greensock/TweenMax.min.js"></script>
        <script src="course/plugins/greensock/TimelineMax.min.js"></script>
        <script src="course/plugins/scrollmagic/ScrollMagic.min.js"></script>
        <script src="course/plugins/greensock/animation.gsap.min.js"></script>
        <script src="course/plugins/greensock/ScrollToPlugin.min.js"></script>
        <script src="course/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
        <script src="course/plugins/scrollTo/jquery.scrollTo.min.js"></script>
        <script src="course/plugins/easing/easing.js"></script>
        <script src="course/js/custom.js"></script>



    </body>
</html>