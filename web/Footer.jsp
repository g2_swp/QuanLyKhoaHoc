<%-- 
    Document   : Footer
    Created on : Sep 23, 2022, 5:13:28 AM
    Author     : MEP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<footer class="footer py-0">
    <div class="container">

        <!-- Footer Content -->

        <div class="footer_content pt-4 pb-3">
            <div class="row">

                <!-- Footer Column - Menu -->

                <div class="col-md-6">
                    <ul class="list-unstyled footer-list text-lg-start text-center mb-0 py-1">
                        <li class="list-inline-item px-2"><a href="terms.html" class="text-foot">Introduction</a></li>|
                        <li class="list-inline-item px-2"><a href="privacy.html" class="text-foot">Q&A</a></li>|
                        <li class="list-inline-item px-2"><a href="aboutus.html" class="text-foot">Privacy Policy</a></li>
                    </ul>
                </div>

                <!-- Footer Column - Usefull Links -->

                <div class="col-lg-4 col-md-3">
                    <div class="list-unstyled text-lg-end text-center py-md-1 py-3">Contact us</div>
                </div>

                <!-- Footer Column - Contact -->

                <div class="col-lg-2 col-md-3">
                    <ul class="list-unstyled social-icon footer-social text-center mb-0">
                        <li class="list-inline-item"><a href="#" class="rounded-pill"><i data-feather="facebook" class="fea icon-sm fea-social"></i></a></li>
                        <li class="list-inline-item"><a href="#" class="rounded-pill"><i data-feather="instagram" class="fea icon-sm fea-social"></i></a></li>
                        <li class="list-inline-item"><a href="#" class="rounded-pill"><i data-feather="twitter" class="fea icon-sm fea-social"></i></a></li>
                        <li class="list-inline-item"><a href="#" class="rounded-pill"><i data-feather="linkedin" class="fea icon-sm fea-social"></i></a></li>
                    </ul><!--end icon-->
                </div>

            </div>
        </div>

        <!-- Footer Content -->

        <div class="footer_content">
            <div class="row">

                <!-- Footer Column - About -->
                <div class="col-lg-3 footer_col">
                    <!-- Logo -->
                    <div class="logo_container">
                        <div class="logo">
                            <img src="course/images/logo.png" alt="">
                            <span>course</span>
                        </div>
                    </div>
                    <p class="footer_about_text">Course Training Support System is a small project for SWP391 created by Group2. Thank you, Mr. KienNT, for letting us borrow the bootstrap template!.</p>
                </div>


                <!-- Footer Column - Contact -->

                <div class="col-lg-3 footer_col">
                    <div class="footer_column_title">About Us</div>
                    <div class="footer_column_content">
                        <ul class="list-unstyled">
                            <li class="footer_contact_item">
                                <div class="footer_contact_icon">
                                    <img src="course/images/placeholder.svg" alt="https://www.flaticon.com/authors/lucy-g">
                                </div>
                                Đại học FPT - Hòa Lạc
                            </li>
                            <li class="footer_contact_item">
                                <div class="footer_contact_icon">
                                    <img src="course/images/smartphone.svg" alt="https://www.flaticon.com/authors/lucy-g">
                                </div>
                                +84 534-468-854
                            </li>
                            <li class="footer_contact_item">
                                <div class="footer_contact_icon">
                                    <img src="course/images/envelope.svg" alt="https://www.flaticon.com/authors/lucy-g">
                                </div>Group2@fpt.edu.vn
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- Footer Column - Menu -->

                <div class="col-lg-6 footer_col">
                    <div class="footer_column_title">Need Help?</div>
                    <form action="SendContact" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <input name="name" type="text" class="form-control border rounded" placeholder="Your name(*)" value="${sessionScope.full_name}"required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <select class="form-select" name="category" required>
                                        <option value="">Select Category</option>
                                        <c:forEach items="${listCate}" var="c">
                                            <option value="${c.setting_id}">${c.setting_title}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div><!--end col-->

                            <div class="col-md-6">
                                <div class="mb-3">
                                    <input name="mobile" id="myform_phone" pattern="[0-9]{10}" type="tel" class="form-control border rounded" placeholder="Phone(*)" value="${sessionScope.mobile}" required>
                                </div> 
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <input name="email" type="email" class="form-control border rounded" placeholder="Email(*)" value="${sessionScope.email}" required>
                                </div> 
                            </div><!--end col-->

                            <div class="col-md-12">
                                <div class="mb-3">
                                    <textarea name="message" rows="4" class="form-control border rounded" placeholder="Your Message" required></textarea>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-warning">Send</button>
                            </div><!--end col-->
                        </div><!--end row-->
                    </form>
                </div>

            </div>
        </div>

    </div>


</footer>
<script>
    var phone_input = document.getElementById("myform_phone");

    phone_input.addEventListener('input', () => {
        phone_input.setCustomValidity('');
        phone_input.checkValidity();
    });

    phone_input.addEventListener('invalid', () => {
        if (phone_input.value === '') {
            phone_input.setCustomValidity('Enter phone number!');
        } else {
            phone_input.setCustomValidity('Enter phone number in this format: 0123-456-789');
        }
    });
</script>

