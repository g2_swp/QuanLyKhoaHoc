<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Edit Subject</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="../../../index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="course/images/logo.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <style>
            i.fa-regular {
                font-family: fontawesome !important;
            }
            .logo span {
                font-family: 'Open Sans', sans-serif;
                font-size: 30px;
                font-weight: 900;
                vertical-align: middle;
                text-transform: uppercase;
                margin-left: 3px;
            }
        </style>

    </head>

    <body class="bg-dark">
        <!-- Loader -->
        <jsp:include page="Loader.jsp"></jsp:include>
            <!-- Loader -->

            <div class="page-wrapper doctris-theme">
                <!-- SideBar -->
            <jsp:include page="AdSideBar.jsp"></jsp:include>
                <!-- SideBar  -->

                <!-- Start Page Content -->
                <main class="page-content bg-light">

                    <!-- Header  -->
                <jsp:include page="AdHeader.jsp"></jsp:include>
                    <!-- Header  -->
                    <section class="bg-dashboard">
                        
                        <div class="container">
                            <p style="color: blue" id="nota">${nota}</p>
                            <div class="rounded shadow mt-4">
                                <div class="p-4 border-bottom">
                                    <h5 class="mb-0">Edit Milestone Details</h5>
                                </div>

                                <div class="p-4">
                                    <b id="nota" style="color: blue;font-family: cursive"></b>
                                    <form id="myForm" action="" method="post">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="form-label">Milestone Id</label>
                                                    <input name="milestoneid" id="milestoneid" type="text" class="form-control" style="background-color: #d4d4d2" value="${milestone.milestone_id}" readonly>
                                            </div>
                                        </div><!--end col-->

                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Assignment Title</label>
                                                <input name="ass" id="ass" type="text" class="form-control" style="background-color: #d4d4d2"  value="${milestone.titleAss}" readonly >
                                            </div>
                                        </div><!--end col-->

                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Class Code</label>
                                                <input name="classcode" id="classcode" type="text" class="form-control" style="background-color: #d4d4d2" value="${milestone.class_code}" readonly>
                                            </div> 
                                        </div><!--end col-->
                                         <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Status</label>
                                                <select name="stt" id="stt" class="form-control">
                                                    <option <c:if test="${milestone.status==1}">selected</c:if> value="1">Active</option>
                                                    <option <c:if test="${milestone.status==0}">selected</c:if> value="0">DeActive</option>
                                                </select>
                                            </div>                                                                               
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">From Date</label>
                                                <input name="fromdate" id="fromdate" type="date" class="form-control"value="${milestone.from_date}">
                                            </div> 
                                        </div><!--end col-->
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">To Date</label>
                                                <input name="name" id="todate" type="date" class="form-control"value="${milestone.to_date}">
                                            </div> 
                                        </div><!--end col-->

                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Ass Body</label>
                                                <textarea class="form-control" id="assbody" name="w3review" rows="4" cols="50">
                                                    ${milestone.ass_body}
                                                </textarea>
                                            </div> 
                                        </div><!--end col-->
                                        
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">Description</label>
                                                <textarea class="form-control" id="des" name="w3review" rows="4" cols="50">
                                                    ${milestone.description}
                                                </textarea>
                                            </div> 
                                        </div><!--end col-->

                                       
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="submit" id="submit" name="send" onclick="saveChange()" class="btn btn-primary" value="Save changes">
                                        </div><!--end col-->
                                    </div><!--end row-->
                                </form><!--end form--> 
                            </div>
                        </div>
                    </div><!--end container-->
                </section><!--end section-->
                <a style="margin-left: 1300px"  class="btn btn-primary" href="milestone">Back List Milestone</a>
                <!-- Footer Start -->
                <jsp:include page="AdFooter.jsp"></jsp:include>
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->

        <!-- javascript -->
        <script src="assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="assets/js/simplebar.min.js"></script>
        <!-- Icons -->
        <script src="assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="assets/js/app.js"></script>
        <script type="text/javascript">
           async function saveChange(){
                event.preventDefault();
                const milestoneid  = document.getElementById('milestoneid').value;
                const fromdate = document.getElementById('fromdate').value;
                const todate = document.getElementById('todate').value;
                const sttus = document.getElementById('stt').value;
                const assbody = document.getElementById('assbody').value;
                const des = document.getElementById('assbody').value;
                 const res = await fetch('editmilestone?milestoneid=' + milestoneid + "&fromdate=" + fromdate + "&todate=" + todate
                                                                    + "&stt=" + sttus + "&ass_body=" + assbody + "&des=" + des , {method: 'post'})
                                                                    .then(res => res.json());
                                                            document.getElementById('nota').innerHTML = res;
            }
        </script>
    </body>

</html>